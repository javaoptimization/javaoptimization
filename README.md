# Read Me First

### Build

`gradle clean build` - to build project

### Run

run main method QuickOptimizationApplication.java

To run with glowroot add to VM options  `-javaagent:glowroot/glowroot.jar`
To change TaskService implementation add to VM options  `-Ddemo.profile=good/bad`

'startup.bat/sh' - start application with h2

To run with mysql, create new database 'task_manager'
'startup_mysql_bad.bat/sh' - start application with mysql and TaskBadService
'startup_mysql_good.bat/sh' - start application with mysql and TaskGoodService

### Usage
http://localhost:9090/ - application
http://localhost:4000/ - glowroot
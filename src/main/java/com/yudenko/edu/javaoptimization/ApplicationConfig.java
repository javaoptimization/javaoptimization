package com.yudenko.edu.javaoptimization;

import com.yudenko.edu.javaoptimization.converter.mappers.ImportTaskModelToTaskMapper;
import com.yudenko.edu.javaoptimization.converter.mappers.TaskCreateDtoToTaskMapper;
import com.yudenko.edu.javaoptimization.converter.mappers.TaskEntityToTaskInfoDtoMapper;
import com.yudenko.edu.javaoptimization.service.UserService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ApplicationConfig {

    @Autowired
    private UserService userService;

    @Bean
    public ModelMapper modelMapper() {
        ModelMapper modelMapper = new ModelMapper();
        modelMapper.addMappings(new TaskEntityToTaskInfoDtoMapper());
        modelMapper.addMappings(new TaskCreateDtoToTaskMapper());
        modelMapper.addMappings(new ImportTaskModelToTaskMapper(userService));
        return modelMapper;
    }
}

package com.yudenko.edu.javaoptimization.service;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

@Service
public class CoreJavaOptimizationService {

    public void testStringIsEmpty(List<String> list) {
        int emptyStringsCount = 0;
        for (int i = 0; i < list.size(); i++) {
            String str = list.get(i);
            if (StringUtils.isEmpty(str)) {
                emptyStringsCount++;
            }
        }
    }

    public void testStringIsBlank(List<String> list) {
        int emptyStringsCount = 0;
        for (int i = 0; i < list.size(); i++) {
            String str = list.get(i);
            if (StringUtils.isBlank(str)) {
                emptyStringsCount++;
            }
        }
    }

    public void testStringIsBlankWithTrim(List<String> list) {
        int emptyStringsCount = 0;
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i) != null) {
                String str = list.get(i).trim();
                if (StringUtils.isBlank(str)) {
                    emptyStringsCount++;
                }
            }
        }
    }

    public void testStringIsEmptyWithTrim(List<String> list) {
        int emptyStringsCount = 0;
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i) != null) {
                String str = list.get(i).trim();
                if (StringUtils.isEmpty(str)) {
                    emptyStringsCount++;
                }
            }
        }
    }

    public void testObjectIsNull(List<String> list) {
        int nullObjectsCount = 0;
        for (int i = 0; i < list.size(); i++) {
            String s = list.get(i);
            if (Objects.isNull(s)) {
                nullObjectsCount++;
            }
        }
    }

    public void testPlainJavaIsNull(List<String> list) {
        int nullObjectsCount = 0;
        for (int i = 0; i < list.size(); i++) {
            String s = list.get(i);
            if (s == null) {
                nullObjectsCount++;
            }
        }
    }
}

package com.yudenko.edu.javaoptimization.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.yudenko.edu.javaoptimization.domain.tasks.JsonTask;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class JsonService {

    public List<String> toJsonPlate(List<JsonTask> tasks) {
        List<String> jsons = new ArrayList<>();
        for (JsonTask task : tasks) {
            jsons.add(task.toJson());
        }
        return jsons;
    }

    public List<String> toJsonJackson(List<JsonTask> tasks) {
        ObjectMapper objectMapper = new ObjectMapper();
        List<String> jsons = new ArrayList<>();
        for (JsonTask task : tasks) {
            try {
                jsons.add(objectMapper.writeValueAsString(task));
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
        }
        return jsons;
    }

    public List<String> toJsonGson(List<JsonTask> tasks) {
        List<String> jsons = new ArrayList<>();
        GsonBuilder builder = new GsonBuilder();
        builder.registerTypeAdapter(Date.class, new DateSerializer());
        Gson mapper = builder.create();
        for (JsonTask task : tasks) {
            jsons.add(mapper.toJson(task));
        }
        return jsons;
    }

    private class DateSerializer implements JsonSerializer<Date> {

        @Override
        public JsonElement serialize(Date src, Type typeOfSrc, JsonSerializationContext context) {
            return src == null ? null : new JsonPrimitive(src.getTime());
        }
    }
}

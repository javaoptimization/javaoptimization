package com.yudenko.edu.javaoptimization.service;

import com.yudenko.edu.javaoptimization.dto.TaskDto;
import org.springframework.stereotype.Service;

import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Date;
import java.util.List;

@Service
public class ValidationService {

    public int validateAPI(List<TaskDto> tasks) {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();

        int countInvalidTasks = 0;
        for (TaskDto task : tasks) {
            if (validator.validate(task).size() > 0) {
                countInvalidTasks++;
            }
        }
        return countInvalidTasks;
    }

    public int validateCustom(List<TaskDto> tasks) {
        int countInvalidTasks = 0;
        for (TaskDto task : tasks) {
            countInvalidTasks += isValid(task);
        }
        return countInvalidTasks;
    }

    private byte isValid(TaskDto task) {
        if (task.getTitle() == null) {
            return 1;
        }
        if (task.getTitle().length() > 15) {
            return 1;
        }

        if (task.getDescription() != null && task.getDescription().length() > 20) {
            return 1;
        }

        if (task.getDeadLine() == null) {
            return 1;
        }
        if (task.getDeadLine().compareTo(new Date()) <= 0) {
            return 1;
        }

        return 0;
    }
}

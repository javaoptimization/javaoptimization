package com.yudenko.edu.javaoptimization.service;

import com.yudenko.edu.javaoptimization.domain.Task;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

@Service
public class HashCodeService {

    public int[] collectHashCodes(List<Task> tasks) {
        int[] hashCodes = new int[tasks.size()];
        int i = 0;
        for (Task task : tasks) {
            hashCodes[i++] = task.hashCode();
        }
        return hashCodes;
    }

    public boolean containsData(List<Task> tasks, Task targetTask) {
        return tasks.contains(targetTask);
    }

    public void fillSet(List<Task> tasks, Set<Task> targetSet) {
        targetSet.addAll(tasks);
    }

    public void callHashCode(Task testTask, int amount) {
        for (int i = 0; i < amount; i++) {
            testTask.hashCode();
        }
    }
}

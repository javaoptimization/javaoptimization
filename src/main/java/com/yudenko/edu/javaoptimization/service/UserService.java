package com.yudenko.edu.javaoptimization.service;

import com.yudenko.edu.javaoptimization.converter.UserConverter;
import com.yudenko.edu.javaoptimization.domain.db.User;
import com.yudenko.edu.javaoptimization.dto.CreateUserRequest;
import com.yudenko.edu.javaoptimization.dto.UserDto;
import com.yudenko.edu.javaoptimization.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private UserConverter userConverter;

    public List<UserDto> findAllUsers() {
        return userConverter.toUserDtos(userRepository.findAll());
    }

    public void createUser(CreateUserRequest createUserRequest) {
        User entity = new User();
        entity.setFirstName(createUserRequest.getFirstName());
        entity.setLastName(createUserRequest.getLastName());
        userRepository.save(entity);
    }

    public User findByFullName(String fullName) {
        String[] name = fullName.split("\\s+");
        return userRepository.findByFullName(name[0], name[1]);
    }
}

package com.yudenko.edu.javaoptimization.service;

import com.yudenko.edu.javaoptimization.AppUtil;
import com.yudenko.edu.javaoptimization.converter.mappers.TaskMapper;
import com.yudenko.edu.javaoptimization.domain.TaskStatus;
import com.yudenko.edu.javaoptimization.domain.db.TaskEntity;
import com.yudenko.edu.javaoptimization.domain.db.User;
import com.yudenko.edu.javaoptimization.dto.ImportTaskDetails;
import com.yudenko.edu.javaoptimization.dto.ImportTaskModel;
import com.yudenko.edu.javaoptimization.dto.TaskCreateDto;
import com.yudenko.edu.javaoptimization.dto.TaskInfoDto;
import com.yudenko.edu.javaoptimization.dto.statistic.StatusInfoDto;
import com.yudenko.edu.javaoptimization.dto.statistic.TaskStatisticDto;
import com.yudenko.edu.javaoptimization.exception.ValidationException;
import com.yudenko.edu.javaoptimization.repository.TaskRepository;
import com.yudenko.edu.javaoptimization.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toList;

@Service("taskBadService")
@ConditionalOnProperty(prefix = "demo", name = "profile", havingValue = "bad")
@Transactional
public class TaskBadService implements TaskService {
    @Autowired
    private TaskRepository taskRepository;

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private UserService userService;
    @Autowired
    private TaskMapper taskMapper;

    @Override
    public void createTask(TaskCreateDto taskCreateDto) {
        // validate
        validateTask(taskCreateDto);
        // create
        TaskEntity newTask = taskMapper.toEntity(taskCreateDto);
        newTask.setCreatedBy(userRepository.getOne(taskCreateDto.getCreatedByUserId()));
        if (taskCreateDto.getAssignedToUserId() != 0) {
            newTask.setAssignedTo(userRepository.getOne(taskCreateDto.getAssignedToUserId()));
        }
        // save
        taskRepository.save(newTask);
    }

    @Override
    public void createTaskForAll(TaskCreateDto taskCreateDto) {
        List<TaskEntity> taskEntities = new ArrayList<>();
        // validate
        validateTask(taskCreateDto);
        // find all users
        List<User> allUsers = userRepository.findAll();
        long createdByUserId = taskCreateDto.getCreatedByUserId();

        allUsers.forEach(targetUser -> {
            TaskEntity newTask = taskMapper.toEntity(taskCreateDto); // object is created even target user == createdBy user
            newTask.setCreatedBy(userRepository.getOne(createdByUserId));
            if (!targetUser.getId().equals(createdByUserId)) {
                newTask.setAssignedTo(targetUser);
                taskEntities.add(newTask);
            }
        });
        taskRepository.saveAll(taskEntities);
    }

    @Override
    public Page<TaskInfoDto> find(long targetUserId, TaskStatus status, Pageable pageable) {
        User user = userRepository.getOne(targetUserId);
        Page<TaskEntity> tasks = status == null ?
                taskRepository.findByAssignedTo(user, pageable) :
                taskRepository.findByAssignedToAndStatus(user, status, pageable);
        // map db to dto
        return tasks.map(taskMapper::toDto);
    }

    @Override
    public TaskStatisticDto buildStatistic(long targetUserId, int maxAmount) {
        Date now = new Date();
        User user = userRepository.getOne(targetUserId);
        List<TaskEntity> tasks = taskRepository.findAllForUser(user);
        Set<StatusInfoDto> statusInfo = getStatusInfo(tasks);
        List<TaskInfoDto> currentTasks = getTaskInfoDtosByStatus(maxAmount, tasks, TaskStatus.IN_PROGRESS);
        List<TaskInfoDto> closedTasks = getTaskInfoDtosByStatus(maxAmount, tasks, TaskStatus.DONE);
        List<TaskInfoDto> prioritized = prepareTaskInfo(tasks, maxAmount, task -> Objects.equals(task.getStatus(), TaskStatus.OPEN), Comparator.comparingInt(TaskEntity::getPriority).reversed());

        Predicate<TaskEntity> deadLinedPredicate = task -> Objects.equals(task.getStatus(), TaskStatus.OPEN)
                && !Objects.isNull(task.getDeadLine()) && task.getDeadLine().after(now);
        List<TaskInfoDto> deadLined = prepareTaskInfo(tasks, maxAmount, deadLinedPredicate, Comparator.comparingLong(task -> task.getDeadLine().getTime()));

        // build result
        TaskStatisticDto statistic = new TaskStatisticDto();
        statistic.setTotal(tasks.size());
        statistic.setStatusInfo(statusInfo);
        statistic.setCurrent(currentTasks);
        statistic.setLastClosed(closedTasks);
        statistic.setPrioritized(prioritized);
        statistic.setDeadLined(deadLined);

        return statistic;
    }

    @Override
    public ImportTaskDetails importViaCsv(long createdByUserId, InputStream file) {
        User createdByUser = userRepository.getOne(createdByUserId);
        List<ImportTaskModel> taskList = AppUtil.load(file, ImportTaskModel.class);
        List<TaskEntity> validatedTasks = validateCSV(taskList);

        validatedTasks.forEach(taskEntity -> {
            taskEntity.setCreatedBy(createdByUser);
            taskEntity.setTitle(taskEntity.getTitle().trim());
            taskEntity.setDescription(taskEntity.getDescription().trim());
        });
        taskRepository.saveAll(validatedTasks);

        return new ImportTaskDetails(taskList.size(), validatedTasks.size());
    }

    private List<TaskEntity> validateCSV(List<ImportTaskModel> importedTasks) {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();
        return importedTasks.stream()
                .filter(task -> validator.validate(task).size() == 0)
                .map(taskMapper::toEntity)
                .filter(task -> Objects.nonNull(task.getAssignedTo()))
                .collect(toList());
    }

    private Set<StatusInfoDto> getStatusInfo(List<TaskEntity> tasks) {
        Map<TaskStatus, StatusInfoDto> statusInfo = Arrays.stream(TaskStatus.values())
                .collect(Collectors.toMap(status -> status, StatusInfoDto::new, (a, b) -> b));
        tasks.forEach(task -> statusInfo.get(task.getStatus()).inc());
        return new HashSet<>(statusInfo.values());
    }

    private List<TaskInfoDto> getTaskInfoDtosByStatus(int maxAmount, List<TaskEntity> tasks, TaskStatus taskStatus) {
        return tasks.stream()
                .filter(task -> Objects.equals(task.getStatus(), taskStatus))
                .limit(maxAmount)
                .map(taskMapper::toDto)
                .collect(toList());
    }

    private List<TaskInfoDto> prepareTaskInfo(List<TaskEntity> tasks, int maxAmount, Predicate<TaskEntity> predicate, Comparator<TaskEntity> comparator) {
        return tasks.stream()
                .filter(predicate)
                .sorted(comparator)
                .limit(maxAmount)
                .map(taskMapper::toDto)
                .collect(toList());
    }

    private void validateTask(TaskCreateDto taskCreateDto) {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();
        if (validator.validate(taskCreateDto).size() > 0) {
            throw new ValidationException();
        }
    }
}

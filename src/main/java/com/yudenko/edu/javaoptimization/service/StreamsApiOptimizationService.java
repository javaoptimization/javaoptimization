package com.yudenko.edu.javaoptimization.service;

import com.yudenko.edu.javaoptimization.converter.ProductConverter;
import com.yudenko.edu.javaoptimization.domain.Product;
import com.yudenko.edu.javaoptimization.domain.ProductPrice;
import com.yudenko.edu.javaoptimization.domain.Task;
import com.yudenko.edu.javaoptimization.generator.Generator;
import com.yudenko.edu.javaoptimization.generator.OnlyStringsGenerationStrategy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.yudenko.edu.javaoptimization.AppUtil.toList;

@Service
public class StreamsApiOptimizationService {
    private ProductConverter productConverter;
    private Generator<String> generator;

    @Autowired
    public StreamsApiOptimizationService(ProductConverter productConverter) {
        this.productConverter = productConverter;
        this.generator = new Generator<String>();
    }

    public void testListCollector(List<String> list) {
        list.stream().map(e -> productConverter.toProduct(e)).collect(Collectors.toList());
    }

    public void testLinkedListCollector(List<String> list) {
        list.stream().map(e -> productConverter.toProduct(e)).collect(Collectors.toCollection(LinkedList::new));
    }

    public void testFixedArrayListCollector(List<String> list) {
        list.stream().map(e -> productConverter.toProduct(e)).collect(toList(list.size()));
    }

    public void testGenerationStringsToArrayList(int amountTestData) {
        generator.generateData(amountTestData, new OnlyStringsGenerationStrategy(), new ArrayList<>());
    }

    public void testGenerationStringsToArrayListWithInitialSize(int amountTestData) {
        generator.generateData(amountTestData, new OnlyStringsGenerationStrategy(), new ArrayList<>(amountTestData));
    }

    public void testGenerationStringsToLinkedList(int amountTestData) {
        generator.generateData(amountTestData, new OnlyStringsGenerationStrategy(), new LinkedList<>());
    }

    public void testOptionalIfPresent(List<Task> generatedTasks) {
        for (Task task : generatedTasks) {
            Optional<Task> optionalTask = Optional.ofNullable(task);
            optionalTask.ifPresent(e -> e.setTitle(String.format("Task #%d", e.getId())));
        }
    }

    public void testPlainJavaIsNull(List<Task> generatedTasks) {
        for (Task task : generatedTasks) {
            if (task != null) {
                task.setTitle(String.format("Task #%d", task.getId()));
            }
        }
    }

    public BigDecimal testOptionalOfNullableOrElseMulti(List<Product> products) {
        return products.stream().map(this::getPrice).map(ProductPrice::getAmount).reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    public BigDecimal testOptionalOfNullableOrElseSingle(List<Product> products) {
        ProductPrice defaultPrice = getDefaultPrice();
        BigDecimal totalPrice = BigDecimal.ZERO;
        for (Product product : products) {
            ProductPrice price = getPriceOrDefault(product, defaultPrice);
            totalPrice = totalPrice.add(price.getAmount());
        }
        return totalPrice;
    }

    public BigDecimal testOptionalOfNullableOrElseLambda(List<Product> products) {
        return products.stream().map(this::getPriceLambda).map(ProductPrice::getAmount).reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    private ProductPrice getPrice(Product product) {
        return Optional.ofNullable(product)
                .map(Product::getPrice)
                .orElse(getDefaultPrice());
    }

    private ProductPrice getPriceLambda(Product product) {
        return Optional.ofNullable(product)
                .map(Product::getPrice)
                .orElseGet(this::getDefaultPrice);
    }

    private ProductPrice getPriceOrDefault(Product product, ProductPrice defaultPrice) {
        if (product == null) {
            return defaultPrice;
        }
        return product.getPrice();
    }

    private ProductPrice getDefaultPrice() {
        // Only imagine how heavy this method can be
        // 1. get data from DB from single table by id - cache is used
        // 2. get data from DB by not id - no cache using
        // 3. calculate some default data based on previous values (static)
        // 4. get data from external service (api) - rest, soap etc.
        return new ProductPrice(BigDecimal.ONE);
    }

}

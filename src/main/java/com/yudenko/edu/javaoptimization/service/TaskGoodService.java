package com.yudenko.edu.javaoptimization.service;

import com.yudenko.edu.javaoptimization.AppUtil;
import com.yudenko.edu.javaoptimization.domain.TaskStatus;
import com.yudenko.edu.javaoptimization.domain.db.TaskEntity;
import com.yudenko.edu.javaoptimization.domain.db.User;
import com.yudenko.edu.javaoptimization.dto.ImportTaskDetails;
import com.yudenko.edu.javaoptimization.dto.ImportTaskModel;
import com.yudenko.edu.javaoptimization.dto.TaskCreateDto;
import com.yudenko.edu.javaoptimization.dto.TaskInfoDto;
import com.yudenko.edu.javaoptimization.dto.UserInfoDto;
import com.yudenko.edu.javaoptimization.dto.statistic.StatusInfoDto;
import com.yudenko.edu.javaoptimization.dto.statistic.TaskStatisticDto;
import com.yudenko.edu.javaoptimization.exception.ValidationException;
import com.yudenko.edu.javaoptimization.repository.TaskRepository;
import com.yudenko.edu.javaoptimization.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import static com.yudenko.edu.javaoptimization.AppUtil.DEFAULT_PRIORITY;
import static com.yudenko.edu.javaoptimization.dto.ImportTaskModel.MAX_SIZE_DESC;
import static com.yudenko.edu.javaoptimization.dto.ImportTaskModel.MAX_SIZE_TITLE;
import static org.apache.commons.lang3.StringUtils.isEmpty;

@Service("taskGoodService")
@ConditionalOnProperty(prefix = "demo", name = "profile", havingValue = "good")
@Transactional
public class TaskGoodService implements TaskService {

    @Autowired
    private TaskRepository taskRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserService userService;

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public void createTask(TaskCreateDto taskCreateDto) {
        // validate
        validateTask(taskCreateDto);
        // create
        TaskEntity newTask = createProtoTask(taskCreateDto);
        if (taskCreateDto.getAssignedToUserId() != 0) {
            newTask.setAssignedTo(userRepository.getOne(taskCreateDto.getAssignedToUserId()));
        }
        // save
        taskRepository.save(newTask);
    }

    @Override
    public void createTaskForAll(TaskCreateDto taskCreateDto) {
        // validate
        validateTask(taskCreateDto);
        // find all users
        List<User> allUsers = userRepository.findAll();
        TaskEntity protoTask = createProtoTask(taskCreateDto);
        // exclude creator
        for (User targetUser : allUsers) {
            if (targetUser.getId() != taskCreateDto.getCreatedByUserId()) {
                // save new task for user
                protoTask.setId(null);
                protoTask.setAssignedTo(targetUser);
                entityManager.detach(taskRepository.save(protoTask));
            }
        }
    }

    @Override
    public Page<TaskInfoDto> find(long targetUserId, TaskStatus status, Pageable pageable) {
        User user = userRepository.getOne(targetUserId);
        Page<TaskEntity> tasks = status == null ?
                taskRepository.findByAssignedTo(user, pageable) :
                taskRepository.findByAssignedToAndStatus(user, status, pageable);
        // map db to dto
        // TODO map target user ones for all tasks to avoid multi creating same object
        List<TaskInfoDto> taskDtos = tasks.get().map(this::taskToDto).collect(AppUtil.toList(tasks.getNumberOfElements()));
        return new PageImpl<>(taskDtos, pageable, tasks.getTotalElements());
        // TODO change to custom streaming to avoid toList collecting
        // return tasks.map(this::taskToDto);
    }

    @Override
    public TaskStatisticDto buildStatistic(long targetUserId, int maxAmount) {
        User user = userRepository.getOne(targetUserId);
        Date now = new Date();
        Map<TaskStatus, StatusInfoDto> statusInfo = new HashMap<>();
        for (TaskStatus status : TaskStatus.values()) {
            statusInfo.put(status, new StatusInfoDto(status));
        }

        List<TaskInfoDto> currentTasks = new ArrayList<>(maxAmount);
        List<TaskInfoDto> closedTasks = new ArrayList<>(maxAmount);
        List<TaskEntity> priorityTasks = new ArrayList<>();
        List<TaskEntity> deadLineTasks = new ArrayList<>();

        List<TaskEntity> tasks = taskRepository.findAllForUser(user);
        for (TaskEntity task : tasks) {
            statusInfo.get(task.getStatus()).inc();
            if (task.getStatus() == TaskStatus.IN_PROGRESS && currentTasks.size() < maxAmount) {
                currentTasks.add(taskToDto(task));
            }
            if (task.getStatus() == TaskStatus.DONE && closedTasks.size() < maxAmount) {
                closedTasks.add(taskToDto(task));
            }
            if (task.getStatus() == TaskStatus.OPEN) {
                priorityTasks.add(task);
                if (task.getDeadLine() != null && task.getDeadLine().after(now)) {
                    deadLineTasks.add(task);
                }
            }
        }

        // build result
        TaskStatisticDto statistic = new TaskStatisticDto();
        statistic.setTotal(tasks.size());
        statistic.setStatusInfo(new HashSet<>(statusInfo.values()));
        statistic.setCurrent(currentTasks);
        statistic.setLastClosed(closedTasks);

        // ideally I would did this on Db server side via query like select top maxAmount * from task where status="Open" order by priority DESC
        List<TaskInfoDto> prioritized = prepareTaskInfo(priorityTasks, maxAmount, Comparator.comparingInt(TaskEntity::getPriority).reversed());
        statistic.setPrioritized(prioritized);

        // ideally I would did this on Db server side via query like select top maxAmount * from task where status="Open" order by dead_line DESC
        List<TaskInfoDto> deadLined = prepareTaskInfo(deadLineTasks, maxAmount, Comparator.comparingLong(task -> task.getDeadLine().getTime()));
        statistic.setDeadLined(deadLined);

        return statistic;
    }

    @Override
    public ImportTaskDetails importViaCsv(long createdByUserId, InputStream file) {
        int createdCount = 0;
        Date now = new Date();
        Map<String, User> nameToUserMap = getNameToUserMap();
        List<ImportTaskModel> importedTasks = getImportTaskModels(file);
        TaskEntity newTask = createImportProtoTask(createdByUserId);
        for (ImportTaskModel importTaskDto : importedTasks) {
            if (isValidTask(importTaskDto, now)) {
                User user = nameToUserMap.get(importTaskDto.getAssignedTo());
                if (user != null) {
                    newTask.setId(null);
                    newTask.setTitle(importTaskDto.getTitle());
                    newTask.setDescription(importTaskDto.getDescription());
                    newTask.setAssignedTo(user);
                    if (importTaskDto.getDeadLine() != null) {
                        newTask.setDeadLine(importTaskDto.getDeadLine());
                    }
                    entityManager.detach(taskRepository.save(newTask));
                    createdCount++;
                }
            }
        }

        return new ImportTaskDetails(importedTasks.size(), createdCount);
    }

    private List<ImportTaskModel> getImportTaskModels(InputStream file) {
        List<ImportTaskModel> importedTasks = AppUtil.load(file, ImportTaskModel.class);
        for (ImportTaskModel task : importedTasks) {
            task.setTitle(task.getTitle().trim());
            task.setDescription(task.getDescription().trim());
        }
        return importedTasks;
    }

    private Map<String, User> getNameToUserMap() {
        List<User> users = userRepository.findAll();
        Map<String, User> nameToUserMap = new HashMap<>();
        for (User user : users) {
            nameToUserMap.put(user.getFullName(), user);
        }
        return nameToUserMap;
    }

    private boolean isValidTask(ImportTaskModel importTask, Date now) {
        return isDeadLineValid(importTask.getDeadLine(), now)
                && isStringValid(importTask.getTitle(), MAX_SIZE_TITLE)
                && isStringValid(importTask.getDescription(), MAX_SIZE_DESC);
    }

    public static boolean isStringValid(String str, int maxSize) {
        return !isEmpty(str) && str.length() <= maxSize;
    }

    public static boolean isDeadLineValid(Date deadLine, Date now) {
        return deadLine == null || deadLine.after(now);
    }

    private List<TaskInfoDto> prepareTaskInfo(List<TaskEntity> tasks, int maxAmount, Comparator<TaskEntity> comparator) {
        return tasks.stream()
                .sorted(comparator)
                .limit(maxAmount)
                .map(this::taskToDto)
                .collect(AppUtil.toList(maxAmount));
    }

    private void validateTask(TaskCreateDto taskCreateDto) {
        if (!isStringValid(taskCreateDto.getTitle(), MAX_SIZE_TITLE)) {
            throw new ValidationException();
        }
        if (!isStringValid(taskCreateDto.getDescription(), MAX_SIZE_DESC)) {
            throw new ValidationException();
        }
        if (taskCreateDto.getCreatedByUserId() == 0) {
            throw new ValidationException();
        }
    }

    private TaskInfoDto taskToDto(TaskEntity task) {
        TaskInfoDto taskInfoDto = new TaskInfoDto();
        taskInfoDto.setId(task.getId());
        taskInfoDto.setCreated(task.getCreated().getTime());
        taskInfoDto.setUpdated(task.getUpdated().getTime());
        taskInfoDto.setDeadLine(task.getDeadLine().getTime());
        taskInfoDto.setTitle(task.getTitle());
        taskInfoDto.setDescription(task.getDescription());
        taskInfoDto.setPriority(task.getPriority());
        taskInfoDto.setStatus(task.getStatus());
        taskInfoDto.setCreatedBy(userToDto(task.getCreatedBy()));
        taskInfoDto.setAssignedTo(userToDto(task.getAssignedTo()));
        return taskInfoDto;
    }

    private UserInfoDto userToDto(User user) {
        UserInfoDto userInfoDto = new UserInfoDto();
        userInfoDto.setId(user.getId());
        userInfoDto.setFullName(user.getFullName());
        return userInfoDto;
    }

    private TaskEntity createProtoTask(TaskCreateDto taskCreateDto) {
        Date now = new Date();
        TaskEntity newTask = new TaskEntity();
        newTask.setTitle(taskCreateDto.getTitle());
        newTask.setDescription(taskCreateDto.getDescription());
        newTask.setStatus(TaskStatus.OPEN);
        newTask.setCreated(now);
        newTask.setUpdated(now);
        if (taskCreateDto.getDeadLine() != 0) {
            newTask.setDeadLine(new Date(taskCreateDto.getDeadLine()));
        }
        newTask.setPriority(taskCreateDto.getPriority());
        newTask.setCreatedBy(userRepository.getOne(taskCreateDto.getCreatedByUserId()));
        return newTask;
    }

    private TaskEntity createImportProtoTask(long createdByUserId) {
        Date now = new Date();
        TaskEntity newTask = new TaskEntity();
        newTask.setStatus(TaskStatus.OPEN);
        newTask.setCreated(now);
        newTask.setUpdated(now);
        newTask.setPriority(DEFAULT_PRIORITY);
        newTask.setCreatedBy(userRepository.getOne(createdByUserId));
        return newTask;
    }
}

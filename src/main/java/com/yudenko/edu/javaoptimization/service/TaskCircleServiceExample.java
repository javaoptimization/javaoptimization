package com.yudenko.edu.javaoptimization.service;

import com.yudenko.edu.javaoptimization.domain.Task;
import com.yudenko.edu.javaoptimization.domain.TaskStatus;
import com.yudenko.edu.javaoptimization.domain.TasksInfo;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class TaskCircleServiceExample {

    public TasksInfo multiCircles(List<Task> tasks, List<TaskStatus> statuses) {
        TasksInfo tasksInfo = new TasksInfo();
        statuses.forEach(status -> {
            tasksInfo.metrics.add(getTasksByStatus(tasks, status));
        });
        return tasksInfo;
    }

    public TasksInfo singleCircle(List<Task> tasks, List<TaskStatus> statuses) {
        TasksInfo tasksInfo = new TasksInfo();
        Map<TaskStatus, List<Task>> resultMap = new HashMap<>(statuses.size());
        for (Task task : tasks) {
            List<Task> targetStatus = resultMap.computeIfAbsent(task.getStatus(), status -> new LinkedList<>());
            targetStatus.add(task);
        }
        tasksInfo.metrics.addAll(resultMap.values());
        return tasksInfo;
    }

    private List<Task> getTasksByStatus(List<Task> tasks, TaskStatus status) {
        return tasks.stream().filter(task -> status == task.getStatus()).collect(Collectors.toList());
    }
}

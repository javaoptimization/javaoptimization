package com.yudenko.edu.javaoptimization.service;

import com.yudenko.edu.javaoptimization.domain.TaskStatus;
import com.yudenko.edu.javaoptimization.dto.ImportTaskDetails;
import com.yudenko.edu.javaoptimization.dto.TaskCreateDto;
import com.yudenko.edu.javaoptimization.dto.TaskInfoDto;
import com.yudenko.edu.javaoptimization.dto.statistic.TaskStatisticDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.io.InputStream;

public interface TaskService {

    void createTask(TaskCreateDto taskCreateDto);

    void createTaskForAll(TaskCreateDto taskCreateDto);

    Page<TaskInfoDto> find(long targetUserId, TaskStatus status, Pageable pageable);

    TaskStatisticDto buildStatistic(long targetUserId, int maxAmount);

    ImportTaskDetails importViaCsv(long createdByUserId, InputStream file);
}

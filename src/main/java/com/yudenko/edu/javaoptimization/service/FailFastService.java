package com.yudenko.edu.javaoptimization.service;

import com.yudenko.edu.javaoptimization.domain.Task;
import com.yudenko.edu.javaoptimization.domain.TaskStatus;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.PredicateUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class FailFastService {

    public List<Task> collectValidFailFast(List<Task> tasks) {
        return tasks.stream()
                .map(this::validateFast)
                .filter(vadlidTask -> vadlidTask != null)
                .collect(Collectors.toCollection(LinkedList::new));
    }

    public List<Task> collectValidFailLater(List<Task> tasks) {
        List<Task> validTasks = tasks.stream().map(this::validateLater).collect(Collectors.toList());
        CollectionUtils.filter(validTasks, PredicateUtils.notNullPredicate());
        return validTasks;
    }

    private Task validateFast(Task task) {
        if (StringUtils.isEmpty(task.getAssignedTo())) {
            return null;
        }
        if (StringUtils.isEmpty(task.getTitle())) {
            return null;
        }

        if (task.getCreated().after(new Date())) {
            return null;
        }
        Task validTask = new Task();
        validTask.setId(task.getId());
        validTask.setTitle(task.getTitle());
        validTask.setCreated(task.getCreated());
        validTask.setStatus(getStatus(task.getId()));
        return validTask;
    }

    private Task validateLater(Task task) {
        Task validTask = new Task(); // why do we create object
        validTask.setId(task.getId());
        validTask.setTitle(task.getTitle());
        validTask.setCreated(task.getCreated());
        validTask.setStatus(getStatus(task.getId())); // why do we get DB for invalid object

        if (StringUtils.isEmpty(task.getAssignedTo())) {
            return null;
        }
        if (StringUtils.isEmpty(task.getTitle())) {
            return null;
        }

        if (task.getCreated().after(new Date())) {
            return null;
        }
        return validTask;
    }

    private TaskStatus getStatus(int taskId) {
        // do some slow work to simulate access to DB
/*
        try {
            Thread.sleep(10);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
*/
        if (taskId % 2 == 0) {
            return TaskStatus.IN_PROGRESS;
        } else {
            return TaskStatus.DONE;
        }
    }

}

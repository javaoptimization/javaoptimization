package com.yudenko.edu.javaoptimization.quiz;

import java.util.ArrayList;
import java.util.List;

public class CommunityService {

    private List<Pupil> mathCommunity = new ArrayList<>();
    private List<Pupil> soccerTeam = new ArrayList<>();
    private List<Pupil> jazBand = new ArrayList<>();
    private List<Pupil> itCommunity = new ArrayList<>();
    private List<Pupil> excellentPupil = new ArrayList<>();

    public int checkCommunityMember(Pupil pupil) {
        int result = 0;
        result += mathCommunity.contains(pupil) ? 1 : 0;
        result += soccerTeam.contains(pupil) ? 1 : 0;
        result += jazBand.contains(pupil) ? 1 : 0;
        result += itCommunity.contains(pupil) ? 1 : 0;
        result += excellentPupil.contains(pupil) ? 1 : 0;
        return result;
    }

    public static class Pupil {
        private String classNumber;
        private String firstName;
        private String lastName;

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Pupil pupil = (Pupil) o;
            return equalsA(pupil);
            // return equalsB(pupil);
            // return equalsC(pupil);
        }

        private boolean equalsA(Pupil pupil) {
            if (!getLastName().equals(pupil.getLastName())) return false;
            if (!getClassNumber().equals(pupil.getClassNumber())) return false;
            if (!getFirstName().equals(pupil.getFirstName())) return false;
            return true;
        }

        private boolean equalsB(Pupil pupil) {
            if (!getFirstName().equals(pupil.getFirstName())) return false;
            if (!getLastName().equals(pupil.getLastName())) return false;
            if (!getClassNumber().equals(pupil.getClassNumber())) return false;
            return true;
        }

        private boolean equalsC(Pupil pupil) {
            if (!getClassNumber().equals(pupil.getClassNumber())) return false;
            if (!getFirstName().equals(pupil.getFirstName())) return false;
            if (!getLastName().equals(pupil.getLastName())) return false;
            return true;
        }

        @Override
        public int hashCode() {
            int result = getClassNumber().hashCode();
            result = 31 * result + getFirstName().hashCode();
            result = 31 * result + getLastName().hashCode();
            return result;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public String getClassNumber() {
            return classNumber;
        }

        public void setClassNumber(String classNumber) {
            this.classNumber = classNumber;
        }
    }

}

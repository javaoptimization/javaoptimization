package com.yudenko.edu.javaoptimization;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableAutoConfiguration
public class QuickOptimizationApplication {

    public static void main(String[] args) {
        SpringApplication.run(QuickOptimizationApplication.class, args);
    }

}

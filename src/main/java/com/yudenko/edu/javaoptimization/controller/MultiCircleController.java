package com.yudenko.edu.javaoptimization.controller;

import com.yudenko.edu.javaoptimization.AppUtil;
import com.yudenko.edu.javaoptimization.domain.Task;
import com.yudenko.edu.javaoptimization.domain.TaskStatus;
import com.yudenko.edu.javaoptimization.dto.ResultExampleDto;
import com.yudenko.edu.javaoptimization.service.TaskCircleServiceExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("circles")
@CrossOrigin
public class MultiCircleController {

    @Autowired
    private TaskCircleServiceExample taskCircleServiceExample;

    private List<Task> allTasks = new ArrayList<>();

    @GetMapping("/{exampleCode}")
    public ResultExampleDto execExample(@PathVariable("exampleCode") String exampleCode, @RequestParam(name = "amount") int amountTestData) {
        // generate data if needed
        if (allTasks.size() != amountTestData) {
            allTasks = generateTasks(amountTestData);
        }
        List<TaskStatus> statuses = Arrays.asList(TaskStatus.DONE, TaskStatus.IN_PROGRESS, TaskStatus.OPEN);
        // run example
        Task.setCallsStatus(0);
        long start = System.nanoTime();
        switch (exampleCode) {
            case "before":
                taskCircleServiceExample.multiCircles(allTasks, statuses);
                break;
            case "after":
                taskCircleServiceExample.singleCircle(allTasks, statuses);
                break;
        }

        long time = System.nanoTime() - start;

        // get result
        ResultExampleDto result = new ResultExampleDto();
        result.setDuration(time);
        result.setHumanDuration(AppUtil.formatNanoSeconds(time));
        result.setExampleName("Circle instead multiply");
        result.setDetails(buildDetails(exampleCode, allTasks.size(), statuses));
        result.setExtraInfo("Status call count: " + Task.getCallsStatus());
        return result;
    }

    @GetMapping("/generate")
    public void generate(@RequestParam(name = "amount") int amountTestData) {
        if (allTasks.size() != amountTestData) {
            allTasks = generateTasks(amountTestData);
        }
    }

    @GetMapping("/clear")
    public void clear() {
        allTasks.clear();
        System.gc();
    }

    private String buildDetails(String exampleCode, long total, List<TaskStatus> statuses) {
        String details = "";
        switch (exampleCode) {
            case "before":
                details = "Multi streaming over same list";
                break;
            case "after":
                details = "Stream once";
                break;
        }
        return String.format("Total tasks: %d, statuses count: %d, statuses: %s, desc: %s", total, statuses.size(), statuses, details);
    }

    private List<Task> generateTasks(int count) {
        int numberStatuses = TaskStatus.values().length;
        List<Task> tasks = new ArrayList<>(count);
        for (int i = 0; i < count; i++) {
            Task task = new Task();
            task.setId(i + 1);
            task.setTitle("Task_" + i);
            int indexStatus = i % numberStatuses;
            task.setStatus(TaskStatus.values()[indexStatus]);
            tasks.add(task);
        }
        return tasks;
    }
}

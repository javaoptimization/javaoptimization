package com.yudenko.edu.javaoptimization.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloWordController {

    @GetMapping("/hello")
    public String hello() {
        return " What would be better Objects.isNull() or != null ? ";
    }
}

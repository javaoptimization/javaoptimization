package com.yudenko.edu.javaoptimization.controller;

import com.yudenko.edu.javaoptimization.AppUtil;
import com.yudenko.edu.javaoptimization.domain.TaskStatus;
import com.yudenko.edu.javaoptimization.dto.ImportTaskDetails;
import com.yudenko.edu.javaoptimization.dto.TaskCreateDto;
import com.yudenko.edu.javaoptimization.dto.TaskInfoDto;
import com.yudenko.edu.javaoptimization.dto.statistic.TaskStatisticDto;
import com.yudenko.edu.javaoptimization.service.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.data.web.SortDefault;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RestController
@RequestMapping("/demo/task")
public class TaskController {

    @Autowired
    private TaskService taskService;

    @PostMapping
    public void createTask(@RequestBody TaskCreateDto taskCreateDto) {
        taskService.createTask(taskCreateDto);
    }

    @PostMapping("/forAll")
    public void createTaskForAllUsers(@RequestBody TaskCreateDto taskCreateDto) {
        taskService.createTaskForAll(taskCreateDto);
    }

    @GetMapping("/{userId}")
    public Page<TaskInfoDto> findAll(@PathVariable("userId") long userId,
                                     @RequestParam("status") TaskStatus status,
                                     @PageableDefault(size = 100) @SortDefault(sort = "priority", direction = Sort.Direction.DESC) Pageable page) {
        return taskService.find(userId, status, page);
    }

    @GetMapping("/{userId}/statistics")
    public TaskStatisticDto getStatistic(@PathVariable("userId") long userId,
                                         @RequestParam(name = "amount", required = false, defaultValue = "10") Integer amount) {
        return taskService.buildStatistic(userId, amount);
    }

    @PostMapping("/import/{userId}")
    public ImportTaskDetails importViaCsv(@PathVariable("userId") long createdByUserId,
                                          @RequestParam("file") MultipartFile file) throws IOException {
        AppUtil.validateCsvFile(file);
        return taskService.importViaCsv(createdByUserId, file.getInputStream());
    }

}

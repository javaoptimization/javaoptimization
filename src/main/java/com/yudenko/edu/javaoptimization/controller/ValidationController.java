package com.yudenko.edu.javaoptimization.controller;

import com.yudenko.edu.javaoptimization.AppUtil;
import com.yudenko.edu.javaoptimization.dto.ResultExampleDto;
import com.yudenko.edu.javaoptimization.dto.TaskDto;
import com.yudenko.edu.javaoptimization.service.ValidationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

@RestController
@RequestMapping("validation")
@CrossOrigin
public class ValidationController {

    @Autowired
    private ValidationService validationService;

    private List<TaskDto> allTasks = new ArrayList<>();

    @GetMapping("/{typeValidation}")
    public ResultExampleDto validateData(@PathVariable("typeValidation") String typeValidation, @RequestParam(name = "amount") int amountTestData) {
        // generate data if needed
        if (allTasks.size() != amountTestData) {
            allTasks = generateTasks(amountTestData);
        }

        int numberInvalidTasks = 0;
        long start = System.nanoTime();
        switch (typeValidation) {
            case "api":
                numberInvalidTasks = validationService.validateAPI(allTasks);
                break;
            case "custom":
                numberInvalidTasks = validationService.validateCustom(allTasks);
                break;
        }
        long time = System.nanoTime() - start;

        // get result
        ResultExampleDto result = new ResultExampleDto();
        result.setDuration(time);
        result.setHumanDuration(AppUtil.formatNanoSeconds(time));
        result.setExampleName("Validation frameworks against plain code.");
        result.setDetails(buildDetails(typeValidation, allTasks.size()));
        result.setExtraInfo("Number of invalid tasks: " + numberInvalidTasks);
        return result;
    }

    private String buildDetails(String type, long total) {
        String details = "";
        switch (type) {
            case "api":
                details = "Use standard validation framework based on annotations.";
                break;
            case "custom":
                details = "Custom boring plain validation.";
                break;
        }
        return String.format("Total tasks: %d, desc: %s", total, details);
    }

    private List<TaskDto> generateTasks(int count) {
        Calendar created = Calendar.getInstance();
        created.add(Calendar.DAY_OF_MONTH, -10);
        Calendar createdFuture = Calendar.getInstance();
        createdFuture.add(Calendar.DAY_OF_MONTH, 10);
        List<TaskDto> tasks = new ArrayList<>(count);
        for (int i = 0; i < count; i++) {
            TaskDto task = new TaskDto();
            task.setId(i + 1);
            task.setTitle("Task_" + i);
            if (i % 5 == 0) {
                if (i % 10 == 0) {
                    task.setTitle("Title is very long and must no pass validation");
                }
            } else {
                task.setTitle("Task_" + i);
            }
            if (i % 25 == 0) {
                if (i % 50 == 0) {
                    task.setDeadLine(created.getTime());
                }
            } else {
                task.setDeadLine(createdFuture.getTime());
            }

            task.setDescription(i % 20 == 0 ? "Description is very long and musr be rejected" : null);
            tasks.add(task);
        }
        return tasks;
    }

}

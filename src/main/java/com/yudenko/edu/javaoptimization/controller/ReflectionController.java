package com.yudenko.edu.javaoptimization.controller;

import com.yudenko.edu.javaoptimization.AppUtil;
import com.yudenko.edu.javaoptimization.domain.TaskStatus;
import com.yudenko.edu.javaoptimization.domain.tasks.JsonTask;
import com.yudenko.edu.javaoptimization.dto.ResultExampleDto;
import com.yudenko.edu.javaoptimization.service.JsonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

@RestController
@RequestMapping("reflection")
@CrossOrigin
public class ReflectionController {

    private List<JsonTask> allTasks = new ArrayList<>();
    @Autowired
    private JsonService jsonService;

    @GetMapping("/json/{type}")
    public ResultExampleDto runJsonExample(@PathVariable("type") String jsonGenerationType, @RequestParam(name = "amount") int amountTestData) {
        // generate data if needed
        if (allTasks.size() != amountTestData) {
            allTasks = null;
            System.gc();
            allTasks = generateTasks(amountTestData);
        }

        long start = System.nanoTime();
        List<String> jsons = null;
        switch (jsonGenerationType) {
            case "Jackson":
                jsons = jsonService.toJsonJackson(allTasks);
                break;
            case "Plate":
                jsons = jsonService.toJsonPlate(allTasks);
                break;
            case "Gson":
                jsons = jsonService.toJsonGson(allTasks);
                break;
        }
        long time = System.nanoTime() - start;
        // get result
        ResultExampleDto result = new ResultExampleDto();
        result.setDuration(time);
        result.setHumanDuration(AppUtil.formatNanoSeconds(time));
        result.setExampleName("Reflection libs against boilerplate code");
        result.setDetails(buildDetails(jsonGenerationType, allTasks.size()));
        return result;
    }

    @GetMapping("/clear")
    public void clear() {
        allTasks = null;
        System.gc();
    }

    private List<JsonTask> generateTasks(int count) {
        Calendar created = Calendar.getInstance();
        created.add(Calendar.DAY_OF_MONTH, -10);
        Calendar updated = Calendar.getInstance();
        int numberStatuses = TaskStatus.values().length;
        List<JsonTask> tasks = new ArrayList<>(count);
        for (int i = 0; i < count; i++) {
            JsonTask task = new JsonTask();
            task.setId(i + 1);
            task.setTitle("Task_" + i);
            int indexStatus = i % numberStatuses;
            task.setStatus(TaskStatus.values()[indexStatus]);
            task.setAssignedTo("Mykola Yudenko");
            task.setCreatedBy("Manager");
            task.setCreated(created.getTime());
            task.setUpdated(updated.getTime());
            task.setDescription("Improve hashCode: part_" + i);
            tasks.add(task);
        }
        return tasks;
    }

    private String buildDetails(String jsonGenerationType, long total) {
        String details = "";
        switch (jsonGenerationType) {
            case "Jackson":
                details = "Jackson is used to serialize";
                break;
            case "Plate":
                details = "Boilerplate code is used";
                break;
            case "Gson":
                details = "Gson lib is used";
                break;
        }
        return String.format("Total tasks: %d, desc: %s", total, details);
    }

}

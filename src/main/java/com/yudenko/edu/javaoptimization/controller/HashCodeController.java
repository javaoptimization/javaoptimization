package com.yudenko.edu.javaoptimization.controller;

import com.yudenko.edu.javaoptimization.AppUtil;
import com.yudenko.edu.javaoptimization.domain.Task;
import com.yudenko.edu.javaoptimization.domain.TaskStatus;
import com.yudenko.edu.javaoptimization.domain.tasks.BuilderTask;
import com.yudenko.edu.javaoptimization.domain.tasks.CachedReflectionTask;
import com.yudenko.edu.javaoptimization.domain.tasks.IdTask;
import com.yudenko.edu.javaoptimization.domain.tasks.IdeaTask;
import com.yudenko.edu.javaoptimization.domain.tasks.ObjectsTask;
import com.yudenko.edu.javaoptimization.domain.tasks.ReflectionTask;
import com.yudenko.edu.javaoptimization.dto.ResultExampleDto;
import com.yudenko.edu.javaoptimization.service.HashCodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("hashcode-equals/{typeHashCode}")
@CrossOrigin
public class HashCodeController {

    @Autowired
    private HashCodeService service;

    private List<Task> allTasks = new ArrayList<>();
    private String currentHashCodeType = "";

    @GetMapping("/hashcode")
    public ResultExampleDto runExampleHashCode(@PathVariable("typeHashCode") String typeHashCode, @RequestParam(name = "amount") int amountTestData) {
        // generate data if needed
        if (allTasks.size() != amountTestData || !currentHashCodeType.equals(typeHashCode)) {
            allTasks = generateTasks(typeHashCode, amountTestData);
            currentHashCodeType = typeHashCode;
        }

        long start = System.nanoTime();

        service.collectHashCodes(allTasks);

        long time = System.nanoTime() - start;

        // get result
        ResultExampleDto result = new ResultExampleDto();
        result.setDuration(time);
        result.setHumanDuration(AppUtil.formatNanoSeconds(time));
        result.setExampleName("HashCode generator");
        result.setDetails(buildDetails(typeHashCode, allTasks.size()));
        return result;
    }

    @GetMapping("/cache")
    public ResultExampleDto runExampleCachedHashCode(@PathVariable("typeHashCode") String typeHashCode, @RequestParam(name = "amount") int amountTestData) {
        // create task for test
        Task targetTask = typeHashCode.equals("cached") || typeHashCode.equals("cachedMutated") ? new CachedReflectionTask() : new ReflectionTask();
        Task.callsHashCode = 0;
        targetTask.setId(1024);
        targetTask.setDescription("Test cached hashcode");
        targetTask.setTitle("Cached Task");
        targetTask.setStatus(TaskStatus.IN_PROGRESS);
        targetTask.setCreatedBy("Somebody");
        targetTask.setAssignedTo("Anybody");
        targetTask.setCreated(new Date());
        targetTask.setUpdated(new Date());

        // call
        long start = System.nanoTime();
        if (typeHashCode.equals("cachedMutated")) {
            service.callHashCode(targetTask, amountTestData/4);
            targetTask.setAssignedTo("FE developer");
            service.callHashCode(targetTask, amountTestData/4);
            targetTask.setStatus(TaskStatus.OPEN);
            service.callHashCode(targetTask, amountTestData/4);
            targetTask.setUpdated(new Date());
            service.callHashCode(targetTask, amountTestData/4);
        } else {
            service.callHashCode(targetTask, amountTestData);
        }
        long time = System.nanoTime() - start;

        // get result
        ResultExampleDto result = new ResultExampleDto();
        result.setDuration(time);
        result.setHumanDuration(AppUtil.formatNanoSeconds(time));
        result.setExampleName("HashCode is cached");
        String details = "";
        switch (typeHashCode) {
            case "cached":
                details = "Cache over reflectioned hashcode.";
                break;
            case "cachedMutated":
                details = "Cache over reflectioned hashcode. Mutation is allowed.";
                break;
            case "Reflection":
                details = "Reflection.";
                break;
        }
        details += " Count of calls: " + amountTestData;
        result.setDetails(details);
        result.setExtraInfo("HashCode calls: " + Task.callsHashCode);
        return result;
    }

    @GetMapping("/equals/{index}")
    public ResultExampleDto runExampleEquals(@PathVariable("typeHashCode") String typeHashCode, @RequestParam(name = "amount") int amountTestData,
                                             @PathVariable("index") int index) {
        // generate data if needed
        if (allTasks.size() != amountTestData || !currentHashCodeType.equals(typeHashCode)) {
            allTasks = generateTasks(typeHashCode, amountTestData);
            currentHashCodeType = typeHashCode;
        }

        Task targetTask = allTasks.get(index);

        long start = System.nanoTime();
        service.containsData(allTasks, targetTask);
        long time = System.nanoTime() - start;

        // get result
        ResultExampleDto result = new ResultExampleDto();
        result.setDuration(time);
        result.setHumanDuration(AppUtil.formatNanoSeconds(time));
        result.setExampleName("Equals generator");
        result.setDetails(buildDetails(typeHashCode, allTasks.size()));
        result.setExtraInfo("Find task in list using equals: " + index);
        return result;
    }

    @GetMapping("/set")
    public ResultExampleDto setExampleRun(@PathVariable("typeHashCode") String typeHashCode,
                                          @RequestParam(name = "amount") int amountTestData,
                                          @RequestParam(name = "initialSize") int initialSetSize) {
        // generate data if needed
        if (allTasks.size() != amountTestData || !currentHashCodeType.equals(typeHashCode)) {
            allTasks = generateTasks(typeHashCode, amountTestData);
            currentHashCodeType = typeHashCode;
        }

        Set<Task> taskSet = initialSetSize == 0 ? new HashSet<>() : new HashSet<>(initialSetSize);
        Task.callsHashCode = 0;
        long start = System.nanoTime();
        service.fillSet(allTasks, taskSet);
        long time = System.nanoTime() - start;
        // get result
        ResultExampleDto result = new ResultExampleDto();
        result.setDuration(time);
        result.setHumanDuration(AppUtil.formatNanoSeconds(time));
        result.setExampleName("Set with different approaches");
        result.setDetails(buildDetails(typeHashCode, allTasks.size()));
        result.setExtraInfo("Initial size of set: " + initialSetSize + ". HashCode calls: " + Task.callsHashCode);
        return result;
    }

    @GetMapping("/clear")
    public void clear() {
        allTasks.clear();
        System.gc();
    }

    private String buildDetails(String hashCodePattern, long total) {
        String details = "No hashcode";
        switch (hashCodePattern) {
            case "Idea":
                details = "Idea generator with using all fields";
                break;
            case "Objects":
                details = "Objects class is used for hashcode and equals";
                break;
            case "Builder":
                details = "Builders are used for hashcode and equals";
                break;
            case "Id":
                details = "Id field is unique and is used for hashcode and equals";
                break;
            case "Reflection":
                details = "Reflection builder is used for hashcode and equals";
                break;
            case "Lombok":
                details = "Lombok @Data is used for hashcode and equals";
                break;
        }
        return String.format("Total tasks: %d, desc: %s", total, details);
    }

    private List<Task> generateTasks(String typeHashCode, int count) {
        Calendar created = Calendar.getInstance();
        created.add(Calendar.DAY_OF_MONTH, -10);
        Calendar updated = Calendar.getInstance();
        int numberStatuses = TaskStatus.values().length;
        List<Task> tasks = new ArrayList<>(count);
        for (int i = 0; i < count; i++) {
            Task task;
            switch (typeHashCode) {
                case "Idea":
                    task = new IdeaTask();
                    break;
                case "Objects":
                    task = new ObjectsTask();
                    break;
                case "Builder":
                    task = new BuilderTask();
                    break;
                case "Id":
                    task = new IdTask();
                    break;
                case "Reflection":
                    task = new ReflectionTask();
                    break;
                default:
                    task = new Task();
            }
            task.setId(i + 1);
            task.setTitle("Task_" + i);
            int indexStatus = i % numberStatuses;
            task.setStatus(TaskStatus.values()[indexStatus]);
            task.setAssignedTo("Mykola Yudenko");
            task.setCreatedBy("Manager");
            task.setCreated(created.getTime());
            task.setUpdated(updated.getTime());
            task.setDescription("Improve hashCode: part_" + i);
            tasks.add(task);
        }
        return tasks;
    }

}

package com.yudenko.edu.javaoptimization.controller;

import com.yudenko.edu.javaoptimization.dto.ResultExampleDto;
import com.yudenko.edu.javaoptimization.generator.BlankStringGenerationStrategy;
import com.yudenko.edu.javaoptimization.generator.BlankWithSymbolsStringGenerationStrategy;
import com.yudenko.edu.javaoptimization.generator.GenerationStrategy;
import com.yudenko.edu.javaoptimization.generator.Generator;
import com.yudenko.edu.javaoptimization.generator.OnlyStringsGenerationStrategy;
import com.yudenko.edu.javaoptimization.generator.StringsAndNullsGenerationStrategy;
import com.yudenko.edu.javaoptimization.service.CoreJavaOptimizationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

import static com.yudenko.edu.javaoptimization.AppUtil.formatNanoSeconds;

@RestController
@RequestMapping("core-java")
@CrossOrigin
public class CoreJavaOptimizationsController {
    @Autowired
    private CoreJavaOptimizationService coreJavaOptimizationService;

    private List<String> generatedStrings;
    private String currentGenerationType = "";

    @GetMapping("/checkForNull/{checkForNullType}")
    public ResultExampleDto checkForNull(@PathVariable("checkForNullType") String checkForNullType,
                                         @RequestParam(name = "amount") int amountTestData) {
        if (generatedStrings == null || generatedStrings.size() != amountTestData) {
            generatedStrings = new Generator<String>().generateData(amountTestData, new OnlyStringsGenerationStrategy(), new ArrayList<>());
        }

        long start = System.nanoTime();

        testIsNull(checkForNullType, generatedStrings);

        long time = System.nanoTime() - start;

        // get result
        ResultExampleDto result = new ResultExampleDto();
        result.setDuration(time);
        result.setHumanDuration(String.valueOf(formatNanoSeconds(time)));
        result.setExampleName("Check object for null");
        result.setDetails(buildDetails(checkForNullType, amountTestData));
        return result;
    }

    @GetMapping("/blank-string/{isEmptyType}")
    public ResultExampleDto runIsEmptyExample(@PathVariable("isEmptyType") String isEmptyType,
                                              @RequestParam("generationType") String generationType,
                                              @RequestParam(name = "amount") int amountTestData) {
        if (generatedStrings == null || generatedStrings.size() != amountTestData || !currentGenerationType.equals(generationType)) {
            GenerationStrategy<String> strategy = getGenerationStrategy(generationType);
            generatedStrings = new Generator<String>().generateData(amountTestData, strategy, new ArrayList<>());
            currentGenerationType = generationType;
        }

        long start = System.nanoTime();

        testIsEmpty(isEmptyType, generatedStrings);

        long time = System.nanoTime() - start;

        // get result
        ResultExampleDto result = new ResultExampleDto();
        result.setDuration(time);
        result.setHumanDuration(String.valueOf(formatNanoSeconds(time)));
        result.setExampleName("Check if string is Empty");
        result.setDetails(buildDetails(isEmptyType, amountTestData));
        result.setExtraInfo(String.format("Generation "));
        return result;
    }

    private void testIsNull(String checkForNullType, List<String> generatedStrings) {
        switch (checkForNullType) {
            case "plain":
                coreJavaOptimizationService.testPlainJavaIsNull(generatedStrings);
                break;
            case "objects":
                coreJavaOptimizationService.testObjectIsNull(generatedStrings);
                break;
        }
    }

    private void testIsEmpty(String isEmptyType, List<String> generatedStrings) {
        switch (isEmptyType) {
            case "isEmpty":
                coreJavaOptimizationService.testStringIsEmpty(generatedStrings);
                break;
            case "isBlank":
                coreJavaOptimizationService.testStringIsBlank(generatedStrings);
                break;
            case "isBlankTrim":
                coreJavaOptimizationService.testStringIsBlankWithTrim(generatedStrings);
                break;
            case "isEmptyTrim":
                coreJavaOptimizationService.testStringIsEmptyWithTrim(generatedStrings);
                break;
        }
    }

    private GenerationStrategy<String> getGenerationStrategy(String generationType) {
        GenerationStrategy<String> generationStrategy = null;
        switch (generationType) {
            case "blanks":
                generationStrategy = new BlankStringGenerationStrategy();
                break;
            case "stringsAndNulls":
                generationStrategy = new StringsAndNullsGenerationStrategy();
                break;
            case "blankWithSymbols":
                generationStrategy = new BlankWithSymbolsStringGenerationStrategy();
                break;
        }
        return generationStrategy;
    }


    private String buildDetails(String testType, long total) {
        String details = "";
        switch (testType) {
            case "plain":
                details = "Plain java 'obj == null' is used";
                break;
            case "objects":
                details = "Objects.isNull method is used";
                break;
            case "isEmpty":
                details = "String.isEmpty method is used";
                break;
            case "isBlank":
                details = "StringUtils.isBlank method is used";
                break;
            case "isBlankTrim":
                details = "StringUtils.isBlank method is used for trimmed string";
                break;
            case "isEmptyTrim":
                details = "StringUtils.isEmpty method is used for trimmed string";
                break;
        }
        return String.format("Total amount of data: %d, desc: %s", total, details);
    }
}

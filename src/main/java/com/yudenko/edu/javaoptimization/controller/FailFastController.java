package com.yudenko.edu.javaoptimization.controller;

import com.yudenko.edu.javaoptimization.AppUtil;
import com.yudenko.edu.javaoptimization.domain.Task;
import com.yudenko.edu.javaoptimization.dto.ResultExampleDto;
import com.yudenko.edu.javaoptimization.service.FailFastService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

@RestController
@RequestMapping("fail")
@CrossOrigin
public class FailFastController {

    @Autowired
    private FailFastService failFastService;
    private List<Task> allTasks = new ArrayList<>();

    @GetMapping("/{type}")
    public ResultExampleDto runExample(@PathVariable("type") String typeFail, @RequestParam(name = "amount") int amountTestData) {
        // generate data if needed
        if (allTasks.size() != amountTestData) {
            allTasks = generateTasks(amountTestData);
        }

        List<Task> validTasks = null;
        long start = System.nanoTime();
        switch (typeFail) {
            case "fast":
                validTasks = failFastService.collectValidFailFast(allTasks);
                break;
            case "later":
                validTasks = failFastService.collectValidFailLater(allTasks);
                break;
        }

        long time = System.nanoTime() - start;

        // get result
        ResultExampleDto result = new ResultExampleDto();
        result.setDuration(time);
        result.setHumanDuration(AppUtil.formatNanoSeconds(time));
        result.setExampleName("Fail fast against fail later");
        result.setDetails(buildDetails(typeFail, allTasks.size()));
        result.setExtraInfo("Valid tasks: " + validTasks.size());
        return result;
    }

    private String buildDetails(String type, long total) {
        String details = "";
        switch (type) {
            case "fast":
                details = "Fail as soon as possible";
                break;
            case "later":
                details = "Fail as later as possible";
                break;
        }
        return String.format("Total tasks: %d, desc: %s", total, details);
    }

    private List<Task> generateTasks(int count) {
        Calendar created = Calendar.getInstance();
        created.add(Calendar.DAY_OF_MONTH, -10);
        Calendar createdFuture = Calendar.getInstance();
        createdFuture.add(Calendar.DAY_OF_MONTH, 10);
        List<Task> tasks = new ArrayList<>(count);
        for (int i = 0; i < count; i++) {
            Task task = new Task();
            task.setId(i + 1);
            task.setTitle(i % 5 == 0 ? "" : "Task_" + i);
            task.setAssignedTo(i % 3 == 0 ? null : "Mykola Yudenko");
            task.setCreatedBy("Manager");
            task.setCreated(i % 10 == 0 ? createdFuture.getTime() : created.getTime());
            task.setDescription("Improve hashCode: part_" + i);
            tasks.add(task);
        }
        return tasks;
    }

}

package com.yudenko.edu.javaoptimization.controller;

import com.yudenko.edu.javaoptimization.dto.CreateUserRequest;
import com.yudenko.edu.javaoptimization.dto.UserDto;
import com.yudenko.edu.javaoptimization.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/users")
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping()
    public List<UserDto> findAllUsers() {
        return userService.findAllUsers();
    }

    @PostMapping()
    public void createUser(@RequestBody @Valid CreateUserRequest request) {
        userService.createUser(request);
    }
}

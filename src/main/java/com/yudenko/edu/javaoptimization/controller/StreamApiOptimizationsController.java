package com.yudenko.edu.javaoptimization.controller;

import com.yudenko.edu.javaoptimization.domain.Product;
import com.yudenko.edu.javaoptimization.domain.ProductPrice;
import com.yudenko.edu.javaoptimization.domain.Task;
import com.yudenko.edu.javaoptimization.dto.ResultExampleDto;
import com.yudenko.edu.javaoptimization.generator.Generator;
import com.yudenko.edu.javaoptimization.generator.OnlyStringsGenerationStrategy;
import com.yudenko.edu.javaoptimization.generator.ProductsOrNullsGenerationStrategy;
import com.yudenko.edu.javaoptimization.generator.TaskOrNullsGenerationStrategy;
import com.yudenko.edu.javaoptimization.service.StreamsApiOptimizationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

import static com.yudenko.edu.javaoptimization.AppUtil.formatNanoSeconds;

@RestController
@RequestMapping("stream-api")
@CrossOrigin
public class StreamApiOptimizationsController {
    @Autowired
    private StreamsApiOptimizationService streamsApiOptimizationService;
    private List<String> generatedStrings;
    private List<Task> generatedTasks;
    private List<Product> generatedProducts;

    @GetMapping("/optionalOfNullableOrElse/{processType}")
    public ResultExampleDto testOptionalOfNullableOrElse(@PathVariable("processType") String processType, @RequestParam(name = "amount") int amountTestData) {

        if (generatedProducts == null || generatedProducts.size() != amountTestData) {
            generatedProducts = new Generator<Product>().generateData(amountTestData, new ProductsOrNullsGenerationStrategy(), new ArrayList<>());
        }
        ProductPrice.pricesCreated = 0;
        Product.callsPrices = 0;
        long start = System.nanoTime();
        switch (processType) {
            case "single":
                streamsApiOptimizationService.testOptionalOfNullableOrElseSingle(generatedProducts);
                break;
            case "multi":
                streamsApiOptimizationService.testOptionalOfNullableOrElseMulti(generatedProducts);
                break;
            case "lambda":
                streamsApiOptimizationService.testOptionalOfNullableOrElseLambda(generatedProducts);
                break;
        }

        long time = System.nanoTime() - start;

        // get result
        ResultExampleDto result = new ResultExampleDto();
        result.setDuration(time);
        result.setHumanDuration(String.valueOf(formatNanoSeconds(time)));
        result.setExampleName("Optional OfNullableOrElse test");
        result.setDetails(buildDetails("ofNullable_" + processType, amountTestData));
        result.setExtraInfo("Amount of calls existing prices: " + Product.callsPrices + ". Count of created ProductPrice objects: " + ProductPrice.pricesCreated);
        return result;
    }

    @GetMapping("/optionalIfPresent/{exampleType}")
    public ResultExampleDto testOptionalIfPresent(@PathVariable("exampleType") String exampleType,
                                                  @RequestParam(name = "amount") int amountTestData) {

        if (generatedTasks == null || generatedTasks.size() != amountTestData) {
            generatedTasks = new Generator<Task>().generateData(amountTestData, new TaskOrNullsGenerationStrategy(), new ArrayList<>());
        }
        long start = System.nanoTime();

        optionalIfPresentTest(exampleType, generatedTasks);

        long time = System.nanoTime() - start;

        // get result
        ResultExampleDto result = new ResultExampleDto();
        result.setDuration(time);
        result.setHumanDuration(String.valueOf(formatNanoSeconds(time)));
        result.setExampleName("Optional IfPresent test");
        result.setDetails(buildDetails(exampleType, amountTestData));
        return result;
    }

    @GetMapping("/data-generation/{containerType}")
    public ResultExampleDto dataGeneration(@PathVariable("containerType") String containerType,
                                           @RequestParam(name = "amount") int amountTestData) {
        long start = System.nanoTime();

        generationDataTest(containerType, amountTestData);

        long time = System.nanoTime() - start;

        // get result
        ResultExampleDto result = new ResultExampleDto();
        result.setDuration(time);
        result.setHumanDuration(String.valueOf(formatNanoSeconds(time)));
        result.setExampleName("Data generation test");
        result.setDetails(buildDetails(containerType, amountTestData));
        return result;
    }

    @GetMapping("/data-collecting/{containerType}")
    public ResultExampleDto dataCollecting(@PathVariable("containerType") String containerType,
                                           @RequestParam(name = "amount") int amountTestData) {
        if (generatedStrings == null || generatedStrings.size() != amountTestData) {
            generatedStrings = new Generator<String>().generateData(amountTestData, new OnlyStringsGenerationStrategy(), new ArrayList<>());
        }

        long start = System.nanoTime();

        collectingTest(containerType, generatedStrings);

        long time = System.nanoTime() - start;

        // get result
        ResultExampleDto result = new ResultExampleDto();
        result.setDuration(time);
        result.setHumanDuration(String.valueOf(formatNanoSeconds(time)));
        result.setExampleName("Data collecting test");
        result.setDetails(buildDetails(containerType, amountTestData));
        return result;
    }

    private void optionalIfPresentTest(String exampleType, List<Task> generatedTasks) {
        switch (exampleType) {
            case "ifPresent":
                streamsApiOptimizationService.testOptionalIfPresent(generatedTasks);
                break;
            case "plain":
                streamsApiOptimizationService.testPlainJavaIsNull(generatedTasks);
                break;
        }
    }

    private void generationDataTest(String containerType, int amountTestData) {
        switch (containerType) {
            case "arrayList":
                streamsApiOptimizationService.testGenerationStringsToArrayList(amountTestData);
                break;
            case "linkedList":
                streamsApiOptimizationService.testGenerationStringsToLinkedList(amountTestData);
                break;
            case "fixedArrayList":
                streamsApiOptimizationService.testGenerationStringsToArrayListWithInitialSize(amountTestData);
                break;
        }
    }

    private void collectingTest(String containerType, List<String> generatedStrings) {
        switch (containerType) {
            case "arrayList":
                streamsApiOptimizationService.testListCollector(generatedStrings);
                break;
            case "linkedList":
                streamsApiOptimizationService.testLinkedListCollector(generatedStrings);
                break;
            case "fixedArrayList":
                streamsApiOptimizationService.testFixedArrayListCollector(generatedStrings);
                break;
        }
    }


    private String buildDetails(String containerType, long total) {
        String details = "";
        switch (containerType) {
            case "arrayList":
                details = "ArrayList was used as container";
                break;
            case "linkedList":
                details = "LinkedList was used as container";
                break;
            case "fixedArrayList":
                details = "ArrayList with initial size was used as container";
                break;
            case "ifPresent":
                details = "Optional.ifPresent method is used";
                break;
            case "plain":
                details = "Plain java 'obj == null' is used";
                break;
            case "ofNullable_single":
                details = "Optional.ofNullable().orElse() uses single default object.";
                break;
            case "ofNullable_multi":
                details = "Optional.ofNullable().orElse() creates default object multi times.";
                break;
            case "ofNullable_lambda":
                details = "Optional.ofNullable().orElseGet() creates default object if absent only.";
                break;
        }
        return String.format("Total amount of data: %d, desc: %s", total, details);
    }
}

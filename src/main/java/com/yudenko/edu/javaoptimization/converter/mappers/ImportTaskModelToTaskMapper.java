package com.yudenko.edu.javaoptimization.converter.mappers;

import com.yudenko.edu.javaoptimization.domain.TaskStatus;
import com.yudenko.edu.javaoptimization.domain.db.TaskEntity;
import com.yudenko.edu.javaoptimization.domain.db.User;
import com.yudenko.edu.javaoptimization.dto.ImportTaskModel;
import com.yudenko.edu.javaoptimization.service.UserService;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.modelmapper.Converter;
import org.modelmapper.PropertyMap;
import org.modelmapper.spi.MappingContext;
import org.springframework.stereotype.Component;

import java.util.Date;

import static com.yudenko.edu.javaoptimization.AppUtil.DEFAULT_PRIORITY;

@Component
@AllArgsConstructor
public class ImportTaskModelToTaskMapper extends PropertyMap<ImportTaskModel, TaskEntity> {

    private final UserService userService;

    @SneakyThrows
    @Override
    protected void configure() {
        Converter<String, User> nameToUserConverter = new Converter<String, User>() {
            @Override
            public User convert(MappingContext<String, User> context) {
                return userService.findByFullName(context.getSource());
            }
        };
        Converter<String, String> stringConverter = new Converter<String, String>() {
            @Override
            public String convert(MappingContext<String, String> context) {
                return context.getSource().trim();
            }
        };

        Date now = new Date();
        map().setId(null);
        using(stringConverter).map(source.getDescription()).setDescription(null);
        using(stringConverter).map(source.getTitle()).setTitle(null);
        using(nameToUserConverter).map(source.getAssignedTo()).setAssignedTo(null);
        map().setCreatedBy(null);
        map().setStatus(TaskStatus.OPEN);
        map().setCreated(now);
        map().setUpdated(now);
        map().setDeadLine(source.getDeadLine());
        map().setPriority(DEFAULT_PRIORITY);
    }
}

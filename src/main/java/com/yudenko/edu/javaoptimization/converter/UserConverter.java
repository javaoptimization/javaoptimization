package com.yudenko.edu.javaoptimization.converter;

import com.yudenko.edu.javaoptimization.domain.db.User;
import com.yudenko.edu.javaoptimization.dto.UserDto;
import org.springframework.stereotype.Component;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class UserConverter {
    public UserDto toUserDto(User entity) {
        UserDto dto = new UserDto();
        dto.setId(entity.getId());
        dto.setFirstName(entity.getFirstName());
        dto.setLastName(entity.getLastName());

        return dto;
    }

    public List<UserDto> toUserDtos(List<User> entities) {

        return entities.stream()
                .map(this::toUserDto)
                .collect(Collectors.toCollection(LinkedList::new));
    }
}

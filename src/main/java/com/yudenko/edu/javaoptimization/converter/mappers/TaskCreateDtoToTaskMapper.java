package com.yudenko.edu.javaoptimization.converter.mappers;

import com.yudenko.edu.javaoptimization.domain.TaskStatus;
import com.yudenko.edu.javaoptimization.domain.db.TaskEntity;
import com.yudenko.edu.javaoptimization.dto.TaskCreateDto;
import org.modelmapper.Condition;
import org.modelmapper.PropertyMap;

import java.util.Date;

public class TaskCreateDtoToTaskMapper extends PropertyMap<TaskCreateDto, TaskEntity> {

    @Override
    protected void configure() {
        Condition<?, ?> deadLineIsNotZero =
                (Condition<Long, Date>) ctx -> ctx.getSource() != 0L;

        Date now = new Date();
        map().setId(null);
        map().setStatus(TaskStatus.OPEN);
        map().setCreated(now);
        map().setUpdated(now);
        when(deadLineIsNotZero).map().setDeadLine(new Date(source.getDeadLine()));
    }

}

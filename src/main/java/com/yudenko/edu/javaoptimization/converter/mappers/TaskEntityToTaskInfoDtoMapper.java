package com.yudenko.edu.javaoptimization.converter.mappers;

import com.yudenko.edu.javaoptimization.domain.db.TaskEntity;
import com.yudenko.edu.javaoptimization.domain.db.User;
import com.yudenko.edu.javaoptimization.dto.TaskInfoDto;
import com.yudenko.edu.javaoptimization.dto.UserInfoDto;
import org.modelmapper.Converter;
import org.modelmapper.PropertyMap;

public class TaskEntityToTaskInfoDtoMapper extends PropertyMap<TaskEntity, TaskInfoDto> {

    @Override
    protected void configure() {
        Converter<User, UserInfoDto> userConverter = new UserToDtoConverter();
        using(userConverter).map(source.getCreatedBy()).setCreatedBy(null);
        using(userConverter).map(source.getAssignedTo()).setAssignedTo(null);
    }
}

package com.yudenko.edu.javaoptimization.converter.mappers;

import com.yudenko.edu.javaoptimization.domain.db.TaskEntity;
import com.yudenko.edu.javaoptimization.dto.ImportTaskModel;
import com.yudenko.edu.javaoptimization.dto.TaskCreateDto;
import com.yudenko.edu.javaoptimization.dto.TaskInfoDto;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.Objects;

@Component
@AllArgsConstructor
public class TaskMapper {

    private final ModelMapper mapper;

    public TaskInfoDto toDto(TaskEntity entity) {
        return Objects.isNull(entity) ? null : mapper.map(entity, TaskInfoDto.class);
    }

    public TaskEntity toEntity(TaskCreateDto dto) {
        return Objects.isNull(dto) ? null : mapper.map(dto, TaskEntity.class);
    }

    public TaskEntity toEntity(ImportTaskModel dto) {
        return mapper.map(dto, TaskEntity.class);
    }

}

package com.yudenko.edu.javaoptimization.converter.mappers;

import com.yudenko.edu.javaoptimization.domain.db.User;
import com.yudenko.edu.javaoptimization.dto.UserInfoDto;
import org.modelmapper.Converter;
import org.modelmapper.spi.MappingContext;

public class UserToDtoConverter implements Converter<User, UserInfoDto> {

    @Override
    public UserInfoDto convert(MappingContext<User, UserInfoDto> context) {
        UserInfoDto userDto = new UserInfoDto();
        User userSource = context.getSource();
        userDto.setId(userSource.getId());
        userDto.setFullName(userSource.getFullName());
        return userDto;
    }
}

package com.yudenko.edu.javaoptimization.converter;

import com.yudenko.edu.javaoptimization.domain.Product;
import org.springframework.stereotype.Component;
@Component
public class ProductConverter {
    public Product toProduct(String description) {
        Product product = new Product();
        product.setDescription(description);

        return product;
    }
}

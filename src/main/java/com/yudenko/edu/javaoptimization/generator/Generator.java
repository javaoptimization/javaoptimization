package com.yudenko.edu.javaoptimization.generator;

import java.util.List;

public class Generator<T> {

    public List<T> generateData(int size, GenerationStrategy<T> generationStrategy, List<T> elements) {
        for (int i = 0; i < size; i++) {
            generationStrategy.generateData(elements);
        }
        return elements;
    }
}

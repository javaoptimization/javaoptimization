package com.yudenko.edu.javaoptimization.generator;

import com.yudenko.edu.javaoptimization.AppUtil;
import com.yudenko.edu.javaoptimization.domain.Product;
import com.yudenko.edu.javaoptimization.domain.ProductPrice;

import java.math.BigDecimal;
import java.util.List;

import static com.yudenko.edu.javaoptimization.AppUtil.getRandomNumberInRange;
import static com.yudenko.edu.javaoptimization.AppUtil.isPrimeNumber;

public class ProductsOrNullsGenerationStrategy implements GenerationStrategy<Product> {

    @Override
    public void generateData(List<Product> elements) {
        if (isPrimeNumber(getRandomNumberInRange(0, 100000))) {
            elements.add(null);
        } else {
            elements.add(givenProduct());
        }
    }

    private Product givenProduct() {
        Product product = new Product();
        product.setDescription(AppUtil.randomString(15));
        ProductPrice productPrice = new ProductPrice(BigDecimal.TEN);
        product.setPrice(productPrice);

        return product;

    }
}

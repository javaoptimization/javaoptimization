package com.yudenko.edu.javaoptimization.generator;

import java.util.List;

public interface GenerationStrategy<T> {
    void generateData(List<T> elements);
}

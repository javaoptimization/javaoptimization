package com.yudenko.edu.javaoptimization.generator;

import java.util.List;

import static com.yudenko.edu.javaoptimization.AppUtil.getRandomNumberInRange;
import static com.yudenko.edu.javaoptimization.AppUtil.isPrimeNumber;
import static com.yudenko.edu.javaoptimization.AppUtil.randomString;

public class StringsAndNullsGenerationStrategy implements GenerationStrategy<String> {

    @Override
    public void generateData(List<String> list) {
        if (isPrimeNumber(getRandomNumberInRange(0, 10000000))) {
            list.add(randomString(10));
        } else {
            list.add(null);
        }
    }
}

package com.yudenko.edu.javaoptimization.generator;

import java.util.List;

import static com.yudenko.edu.javaoptimization.AppUtil.randomString;

public class OnlyStringsGenerationStrategy implements GenerationStrategy<String> {

    @Override
    public void generateData(List<String> list) {
        list.add(randomString(10));
    }
}

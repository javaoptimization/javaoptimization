package com.yudenko.edu.javaoptimization.generator;

import java.util.List;

import static com.yudenko.edu.javaoptimization.AppUtil.getBlanks;
import static com.yudenko.edu.javaoptimization.AppUtil.getRandomNumberInRange;
import static com.yudenko.edu.javaoptimization.AppUtil.isPrimeNumber;
import static com.yudenko.edu.javaoptimization.AppUtil.randomString;

public class BlankWithSymbolsStringGenerationStrategy implements GenerationStrategy<String> {

    @Override
    public void generateData(List<String> list) {
        String res = "";
        if (isPrimeNumber(getRandomNumberInRange(0, 10000000))) {
            res = getBlanks(getRandomNumberInRange(1, 1000)) + randomString(10);
        }
        list.add(res);
    }
}

package com.yudenko.edu.javaoptimization.generator;

import com.yudenko.edu.javaoptimization.domain.Task;

import java.util.Calendar;
import java.util.List;

import static com.yudenko.edu.javaoptimization.AppUtil.getRandomNumberInRange;
import static com.yudenko.edu.javaoptimization.AppUtil.isPrimeNumber;

public class TaskOrNullsGenerationStrategy implements GenerationStrategy<Task> {

    @Override
    public void generateData(List<Task> elements) {
        if (isPrimeNumber(getRandomNumberInRange(0, 10000000))) {
            elements.add(givenTask());
        } else {
            elements.add(null);
        }
    }

    private Task givenTask() {
        Calendar created = Calendar.getInstance();
        created.add(Calendar.DAY_OF_MONTH, -10);
        Calendar createdFuture = Calendar.getInstance();
        createdFuture.add(Calendar.DAY_OF_MONTH, 10);

        Task task = new Task();
        int id = getRandomNumberInRange(0, 1000);
        task.setId(id);
        task.setTitle("Task_" + id);
        task.setAssignedTo("Oleksandr Erzhanov");
        task.setCreatedBy("Manager");
        task.setCreated(createdFuture.getTime());
        task.setDescription("Improve hashCode: part_" + id);
        return task;

    }
}

package com.yudenko.edu.javaoptimization.dto;

import com.yudenko.edu.javaoptimization.domain.TaskStatus;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TaskInfoDto {
    private long id;
    private String title;
    private String description;
    private long created;
    private long updated;
    private long deadLine;
    private UserInfoDto createdBy;
    private UserInfoDto assignedTo;
    private TaskStatus status;
    private int priority;
}

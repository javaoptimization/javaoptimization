package com.yudenko.edu.javaoptimization.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class ImportTaskDetails {
    private int initialCount;
    private int importedCount;
}

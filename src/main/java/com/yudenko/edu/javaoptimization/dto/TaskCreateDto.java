package com.yudenko.edu.javaoptimization.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import static com.yudenko.edu.javaoptimization.dto.ImportTaskModel.MAX_SIZE_DESC;
import static com.yudenko.edu.javaoptimization.dto.ImportTaskModel.MAX_SIZE_TITLE;

@Getter
@Setter
public class TaskCreateDto {
    @NotBlank
    @Size(max = MAX_SIZE_TITLE)
    private String title;
    @NotBlank
    @Size(max = MAX_SIZE_DESC)
    private String description;
    private String category;
    @Min(value = 1L)
    private long createdByUserId;
    private long assignedToUserId;
    private long deadLine;
    private int priority;
}

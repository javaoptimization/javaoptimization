package com.yudenko.edu.javaoptimization.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserInfoDto {
    private long id;
    private String fullName;
}

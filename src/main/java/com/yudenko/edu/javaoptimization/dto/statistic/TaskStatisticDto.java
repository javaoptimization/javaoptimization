package com.yudenko.edu.javaoptimization.dto.statistic;

import com.yudenko.edu.javaoptimization.dto.TaskInfoDto;
import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.Set;

@Getter
@Setter
public class TaskStatisticDto {
    private long total;
    private Set<StatusInfoDto> statusInfo;
    private List<TaskInfoDto> lastClosed;
    private List<TaskInfoDto> current;
    private List<TaskInfoDto> prioritized;
    private List<TaskInfoDto> deadLined;
}

package com.yudenko.edu.javaoptimization.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Future;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.Date;

import static com.yudenko.edu.javaoptimization.dto.ImportTaskModel.ASSIGNED_TO_COLUMN_NAME;
import static com.yudenko.edu.javaoptimization.dto.ImportTaskModel.DEADLINE_COLUMN_NAME;
import static com.yudenko.edu.javaoptimization.dto.ImportTaskModel.DESC_COLUMN_NAME;
import static com.yudenko.edu.javaoptimization.dto.ImportTaskModel.TITLE_COLUMN_NAME;

@Getter
@Setter
@JsonPropertyOrder({TITLE_COLUMN_NAME, DESC_COLUMN_NAME, ASSIGNED_TO_COLUMN_NAME, DEADLINE_COLUMN_NAME})
public class ImportTaskModel {
    final static String TITLE_COLUMN_NAME = "title";
    final static String DESC_COLUMN_NAME = "description";
    final static String ASSIGNED_TO_COLUMN_NAME = "assignedTo";
    final static String DEADLINE_COLUMN_NAME = "deadLine";
    public static final int MAX_SIZE_TITLE = 50;
    public static final int MAX_SIZE_DESC = 255;

    @JsonProperty(TITLE_COLUMN_NAME)
    @NotBlank
    @Size(max = MAX_SIZE_TITLE)
    private String title;

    @JsonProperty(DESC_COLUMN_NAME)
    @NotBlank
    @Size(max = MAX_SIZE_DESC)
    private String description;

    @JsonProperty(ASSIGNED_TO_COLUMN_NAME)
    private String assignedTo;

    @Future
    @JsonProperty(DEADLINE_COLUMN_NAME)
    @JsonFormat(pattern = "MM/dd/yyyy")
    private Date deadLine;
}

package com.yudenko.edu.javaoptimization.dto.statistic;

import com.yudenko.edu.javaoptimization.domain.TaskStatus;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(of = "status")
public class StatusInfoDto {
    private TaskStatus status;
    private int count;

    public StatusInfoDto(TaskStatus status) {
        this.status = status;
    }

    public TaskStatus getStatus() {
        return status;
    }

    public int getCount() {
        return count;
    }

    public void inc() {
        count++;
    }

}

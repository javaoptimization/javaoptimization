package com.yudenko.edu.javaoptimization.domain.tasks;

import com.yudenko.edu.javaoptimization.domain.Task;

public class JsonTask extends Task {

    public String toJson() {
        StringBuilder json = new StringBuilder();
        json.append("{");
        json.append("\"id\":").append(getId()).append(",");
        json.append("\"status\":").append(wrapWithQuote(getStatus())).append(",");
        json.append("\"title\":").append(wrapWithQuote(getTitle())).append(",");
        json.append("\"description\":").append(wrapWithQuote(getDescription())).append(",");
        json.append("\"createdBy\":").append(wrapWithQuote(getCreatedBy())).append(",");
        json.append("\"assignedTo\":").append(wrapWithQuote(getAssignedTo())).append(",");
        json.append("\"created\":").append(getCreated().getTime()).append(",");
        json.append("\"updated\":").append(getUpdated().getTime());
        json.append("}");
        return json.toString();
    }

    private String wrapWithQuote(Object value) {
        return "\"" + value + "\"";
    }
}

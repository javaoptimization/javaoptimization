package com.yudenko.edu.javaoptimization.domain.tasks;

import com.yudenko.edu.javaoptimization.domain.Task;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode
@Data
public class LombokTask extends Task {
    // it will be a lot of getters and setters

// here we can open links to original Lombok site to demonstrate what is generated when we use annotations
// https://projectlombok.org/features/EqualsAndHashCode
// https://projectlombok.org/features/Data
// You need ony getter and setter - do not use @Data

}

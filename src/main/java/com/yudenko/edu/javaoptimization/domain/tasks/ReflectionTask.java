package com.yudenko.edu.javaoptimization.domain.tasks;

import com.yudenko.edu.javaoptimization.domain.Task;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public class ReflectionTask extends Task {

    @Override
    public int hashCode() {
        callsHashCode++;
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object o) {
        return EqualsBuilder.reflectionEquals(this, o);
    }
}

package com.yudenko.edu.javaoptimization.domain.tasks;

import com.yudenko.edu.javaoptimization.domain.TaskStatus;

import java.util.Date;

public class CachedReflectionTask extends ReflectionTask {
    private int hashcode;

    @Override
    public int hashCode() {
        if (hashcode == 0) {
            hashcode = super.hashCode();
        }
        return hashcode;
    }

    @Override
    public void setAssignedTo(String assignedTo) {
        hashcode = 0;
        super.setAssignedTo(assignedTo);
    }

    @Override
    public void setUpdated(Date updated) {
        hashcode = 0;
        super.setUpdated(updated);
    }

    @Override
    public void setStatus(TaskStatus status) {
        hashcode = 0;
        super.setStatus(status);
    }
}

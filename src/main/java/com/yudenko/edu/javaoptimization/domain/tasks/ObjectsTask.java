package com.yudenko.edu.javaoptimization.domain.tasks;

import com.yudenko.edu.javaoptimization.domain.Task;

import java.util.Objects;

public class ObjectsTask extends Task{

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Task task = (Task) o;
        return getId() == task.getId() &&
                Objects.equals(getTitle(), task.getTitle()) &&
                Objects.equals(getDescription(), task.getDescription()) &&
                Objects.equals(getCreated(), task.getCreated()) &&
                Objects.equals(getUpdated(), task.getUpdated()) &&
                getStatus() == task.getStatus() &&
                Objects.equals(getCreatedBy(), task.getCreatedBy()) &&
                Objects.equals(getAssignedTo(), task.getAssignedTo());
    }

    @Override
    public int hashCode() {
        callsHashCode++;
        return Objects.hash(getId(), getTitle(), getDescription(), getCreated(), getUpdated(), getStatus(), getCreatedBy(), getAssignedTo());
    }

}

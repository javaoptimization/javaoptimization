package com.yudenko.edu.javaoptimization.domain.tasks;

import com.yudenko.edu.javaoptimization.domain.Task;

public class IdeaTask extends Task {

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Task task = (Task) o;

        if (getId() != task.getId()) return false;
        if (getTitle() != null ? !getTitle().equals(task.getTitle()) : task.getTitle() != null) return false;
        if (getDescription() != null ? !getDescription().equals(task.getDescription()) : task.getDescription() != null)
            return false;
        if (getCreated() != null ? !getCreated().equals(task.getCreated()) : task.getCreated() != null) return false;
        if (getUpdated() != null ? !getUpdated().equals(task.getUpdated()) : task.getUpdated() != null) return false;
        if (getStatus() != task.getStatus()) return false;
        if (getCreatedBy() != null ? !getCreatedBy().equals(task.getCreatedBy()) : task.getCreatedBy() != null)
            return false;
        return getAssignedTo() != null ? getAssignedTo().equals(task.getAssignedTo()) : task.getAssignedTo() == null;
    }

    @Override
    public int hashCode() {
        callsHashCode++;
        int result = getId();
        result = 31 * result + (getTitle() != null ? getTitle().hashCode() : 0);
        result = 31 * result + (getDescription() != null ? getDescription().hashCode() : 0);
        result = 31 * result + (getCreated() != null ? getCreated().hashCode() : 0);
        result = 31 * result + (getUpdated() != null ? getUpdated().hashCode() : 0);
        result = 31 * result + (getStatus() != null ? getStatus().hashCode() : 0);
        result = 31 * result + (getCreatedBy() != null ? getCreatedBy().hashCode() : 0);
        result = 31 * result + (getAssignedTo() != null ? getAssignedTo().hashCode() : 0);
        return result;
    }
}

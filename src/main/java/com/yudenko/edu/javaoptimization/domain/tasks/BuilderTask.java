package com.yudenko.edu.javaoptimization.domain.tasks;

import com.yudenko.edu.javaoptimization.domain.Task;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public class BuilderTask extends Task {

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        Task task = (Task) o;

        return new EqualsBuilder()
                .append(getId(), task.getId())
                .append(getTitle(), task.getTitle())
                .append(getDescription(), task.getDescription())
                .append(getCreated(), task.getCreated())
                .append(getUpdated(), task.getUpdated())
                .append(getStatus(), task.getStatus())
                .append(getCreatedBy(), task.getCreatedBy())
                .append(getAssignedTo(), task.getAssignedTo())
                .isEquals();
    }

    @Override
    public int hashCode() {
        callsHashCode++;
        return new HashCodeBuilder(17, 37)
                .append(getId())
                .append(getTitle())
                .append(getDescription())
                .append(getCreated())
                .append(getUpdated())
                .append(getStatus())
                .append(getCreatedBy())
                .append(getAssignedTo())
                .toHashCode();
    }

}

package com.yudenko.edu.javaoptimization.domain.tasks;

import com.yudenko.edu.javaoptimization.domain.Task;

public class IdTask extends Task {

    @Override
    public int hashCode() {
        callsHashCode++;
        return getId();
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) {
            return false;
        }
        if (o.getClass() != getClass()) {
            return false;
        }
        return getId() == ((Task) o).getId();
    }
}

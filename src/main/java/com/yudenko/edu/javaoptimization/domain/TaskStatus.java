package com.yudenko.edu.javaoptimization.domain;

public enum TaskStatus {
    OPEN, IN_PROGRESS, DONE
}

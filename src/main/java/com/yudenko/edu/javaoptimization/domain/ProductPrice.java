package com.yudenko.edu.javaoptimization.domain;

import com.yudenko.edu.javaoptimization.AppUtil;

import java.math.BigDecimal;

public class ProductPrice {
    int id;
    BigDecimal amount;
    public static long pricesCreated;

    public ProductPrice(BigDecimal amount) {
        pricesCreated++;
        this.id = AppUtil.getRandomNumberInRange(1, 100000);
        this.amount = amount;
    }

    public BigDecimal getAmount() {
        return amount;
    }
}

package com.yudenko.edu.javaoptimization.domain;

import java.util.Date;

public class Task {
    private int id;
    private String title;
    private String description;
    private Date created;
    private Date updated;
    private TaskStatus status;
    private String createdBy;
    private String assignedTo;
    public static long callsStatus;
    public static long callsHashCode;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    public TaskStatus getStatus() {
        callsStatus++;
        /*
        try {
            Thread.sleep(10);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
*/
        return status;
    }

    public void setStatus(TaskStatus status) {
        this.status = status;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getAssignedTo() {
        return assignedTo;
    }

    public void setAssignedTo(String assignedTo) {
        this.assignedTo = assignedTo;
    }

    public static long getCallsStatus() {
        return callsStatus;
    }

    public static void setCallsStatus(long callsStatus) {
        Task.callsStatus = callsStatus;
    }


    @Override
    public int hashCode() {
        callsHashCode++;
        return super.hashCode();
    }
}

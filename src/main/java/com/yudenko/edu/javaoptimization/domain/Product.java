package com.yudenko.edu.javaoptimization.domain;

public class Product {
    private String description;
    private ProductPrice price;
    public static long callsPrices;

    public ProductPrice getPrice() {
        callsPrices++;
        return price;
    }

    public void setPrice(ProductPrice price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}

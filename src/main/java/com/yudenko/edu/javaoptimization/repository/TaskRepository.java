package com.yudenko.edu.javaoptimization.repository;

import com.yudenko.edu.javaoptimization.domain.TaskStatus;
import com.yudenko.edu.javaoptimization.domain.db.TaskEntity;
import com.yudenko.edu.javaoptimization.domain.db.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface TaskRepository extends CrudRepository<TaskEntity, Long> {

    @Query("SELECT t FROM TaskEntity t WHERE t.assignedTo = :assignedTo AND t.status = :status")
    Page<TaskEntity> findByAssignedToAndStatus(@Param("assignedTo") User assignedToUser, @Param("status") TaskStatus status, Pageable pageable);

    @Query("SELECT t FROM TaskEntity t WHERE t.assignedTo = :assignedTo")
    Page<TaskEntity> findByAssignedTo(@Param("assignedTo") User assignedToUser, Pageable pageable);

    @Query("SELECT t FROM TaskEntity t WHERE t.assignedTo = :assignedTo ORDER BY t.status, t.updated DESC")
    List<TaskEntity> findAllForUser(@Param("assignedTo") User assignedToUser);
}

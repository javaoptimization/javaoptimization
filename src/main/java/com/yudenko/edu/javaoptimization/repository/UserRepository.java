package com.yudenko.edu.javaoptimization.repository;

import com.yudenko.edu.javaoptimization.domain.db.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;


public interface UserRepository extends JpaRepository<User, Long> {

    @Query("SELECT u FROM User u WHERE u.firstName = :firstName AND u.lastName = :lastName")
    User findByFullName(@Param("firstName") String firstName, @Param("lastName") String lastName);
}

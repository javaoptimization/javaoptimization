package com.yudenko.edu.javaoptimization;

import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvParser;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import liquibase.util.file.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.RandomStringGenerator;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Random;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public final class AppUtil {
    public static final int DEFAULT_PRIORITY = 1;
    public static final String CSV_EXTENSION = "csv";
    private static DecimalFormat decimalFormat = new DecimalFormat("0.0000000000");

    private AppUtil() {
        throw new java.lang.UnsupportedOperationException("This is a utility class and cannot be instantiated");
    }

    public static <T> Collector<T, ?, List<T>> toList(int size) {
        return Collectors.toCollection(() -> new ArrayList<T>(size));
    }

    public static boolean isPrimeNumber(int number) {
        if (number <= 1) {
            return false;
        }
        for (int i = 2; i < Math.sqrt(number); i++) {
            if (number % i == 0) {
                return false;
            }
        }
        return true;
    }

    public static int getRandomNumberInRange(int min, int max) {
        Random r = new Random();
        return r.ints(min, (max + 1)).limit(1).findFirst().getAsInt();
    }

    public static String randomString(int size) {
        return new RandomStringGenerator.Builder()
                .withinRange('a', 'z').build()
                .generate(size);
    }

    public static String formatNanoSeconds(long nano) {
        double seconds = (double) nano / 1_000_000_000;
        return decimalFormat.format(seconds);
    }

    public static String getBlanks(int numberOfBlanks) {
        return StringUtils.leftPad("", numberOfBlanks);
    }

    public static void validateCsvFile(MultipartFile file) {
        if (Objects.isNull(file) || file.isEmpty()
                || !CSV_EXTENSION.equalsIgnoreCase(FilenameUtils.getExtension(file.getOriginalFilename()))) {
            throw new IllegalArgumentException("Invalid csv file");
        }
    }

    public static <T> List<T> load(InputStream inputStream, Class<T> clazz) {
        CsvMapper mapper = new CsvMapper();
        mapper.enable(CsvParser.Feature.FAIL_ON_MISSING_COLUMNS);

        List<T> rows;
        try {
            CsvSchema schema = mapper.schemaFor(clazz).withSkipFirstDataRow(true);
            MappingIterator<T> it = mapper.readerFor(clazz).with(schema).readValues(inputStream);
            rows = it.readAll();
        } catch (IOException exception) {
            throw new IllegalArgumentException("Invalid csv file", exception);
        }
        return rows;
    }

}

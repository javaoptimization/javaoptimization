/**
 * GETs a url and returns the contents
 * @param {string} url The URL to fetch
 */
const ajax = (url) =>
	new Promise((resolve, reject) => {
		try {
			const xhttp = new XMLHttpRequest()
			xhttp.onreadystatechange = function () {
				if (this.readyState === 4 && this.status === 200)
					resolve(this.responseText)
			}
			xhttp.onerror = function (ev) {
				console.log(ev)
				console.log(this)
				reject()
			}
			xhttp.open("GET", url, true)
			xhttp.setRequestHeader("Content-Type", "application/json")
			xhttp.send()
		} catch (error) {
			reject(error)
		}
	})
class JavaAPI {
	local = true
	constructor() {
		this.local = location.href.includes("5500")
	}
	/**
	 * Tests circles before and after optimization
	 * @param {number} amount The amount of circles to test
	 * @param {string}
	 */
	async circles(amount, only) {
		const baseURL = this.local ? `http://localhost:9090/circles` : "circles"
		let before = null,
			after = null
		switch (only) {
			case "before":
				before = JSON.parse(await ajax(`${baseURL}/before?amount=${amount}`))
				break
			case "after":
				after = JSON.parse(await ajax(`${baseURL}/after?amount=${amount}`))
				break
			default:
				before = JSON.parse(await ajax(`${baseURL}/before?amount=${amount}`))
				after = JSON.parse(await ajax(`${baseURL}/after?amount=${amount}`))
				break
		}
		return { before, after }
	}
	async hashcode(amount, name) {
		const baseURL = this.local
			? `http://localhost:9090/hashcode-equals`
			: "hashcode-equals"
		return {
			value: JSON.parse(
				await ajax(`${baseURL}/${name}/hashcode?amount=${amount}`)
			),
			arguments,
		}
	}
	async hashcodeEquals(amount, index, name) {
		const baseURL = this.local
			? `http://localhost:9090/hashcode-equals`
			: "hashcode-equals"
		return {
			value: JSON.parse(
				await ajax(`${baseURL}/${name}/equals/${index}?amount=${amount}`)
			),
			arguments,
		}
	}
	async fail(amount, only) {
		const baseURL = this.local ? `http://localhost:9090/fail` : "fail"
		let before = null,
			after = null
		switch (only) {
			case "later":
				before = JSON.parse(await ajax(`${baseURL}/later?amount=${amount}`))
				break
			case "fast":
				after = JSON.parse(await ajax(`${baseURL}/fast?amount=${amount}`))
				break
			default:
				before = JSON.parse(await ajax(`${baseURL}/later?amount=${amount}`))
				after = JSON.parse(await ajax(`${baseURL}/fast?amount=${amount}`))
				break
		}
		return { before, after }
	}
	async validation(amount, only) {
		const baseURL = this.local
			? `http://localhost:9090/validation`
			: "validation"
		let before = null,
			after = null
		switch (only) {
			case "api":
				before = JSON.parse(await ajax(`${baseURL}/api?amount=${amount}`))
				break
			case "custom":
				after = JSON.parse(await ajax(`${baseURL}/custom?amount=${amount}`))
				break
			default:
				before = JSON.parse(await ajax(`${baseURL}/api?amount=${amount}`))
				after = JSON.parse(await ajax(`${baseURL}/custom?amount=${amount}`))
				break
		}
		return { before, after }
	}
	async cachedHashcode(amount, only) {
		const baseURL = this.local
			? `http://localhost:9090/hashcode-equals`
			: "hashcode-equals"
		let before = null,
			after = null,
			example = null
		switch (only) {
			case "reflection":
				example = JSON.parse(
					await ajax(`${baseURL}/Reflection/cache?amount=${amount}`)
				)
				break
			case "cached":
				before = JSON.parse(
					await ajax(`${baseURL}/cached/cache?amount=${amount}`)
				)
				break
			case "cached mutated":
				after = JSON.parse(
					await ajax(`${baseURL}/cachedMutated/cache?amount=${amount}`)
				)
				break
		}
		return { example, before, after }
	}
	async json(amount, only) {
		const baseURL = this.local
			? `http://localhost:9090/reflection/json`
			: "reflection/json"
		return {
			value: JSON.parse(await ajax(`${baseURL}/${only}?amount=${amount}`)),
			arguments,
		}
	}
	async objectIsNull(amount, only) {
		const baseURL = this.local
			? `http://localhost:9090/core-java/checkForNull`
			: "core-java/checkForNull"
		let before = null,
			after = null
		switch (only) {
			case "before":
				before = JSON.parse(await ajax(`${baseURL}/objects?amount=${amount}`))
				break
			case "after":
				after = JSON.parse(await ajax(`${baseURL}/plain?amount=${amount}`))
				break
			default:
				before = JSON.parse(await ajax(`${baseURL}/objects?amount=${amount}`))
				after = JSON.parse(await ajax(`${baseURL}/plain?amount=${amount}`))
				break
		}
		return { before, after }
	}
	async genArray(amount, only) {
		const baseURL = this.local
			? "http://localhost:9090/stream-api/data-generation"
			: "stream-api/data-generation"
		let value
		switch (only) {
			case "arraylist":
				value = await ajax(`${baseURL}/arrayList?amount=${amount}`)
				break
			case "fixedarraylist":
				value = await ajax(`${baseURL}/fixedArrayList?amount=${amount}`)
				break
			case "linkedlist":
				value = await ajax(`${baseURL}/linkedList?amount=${amount}`)
				break
		}
		value = JSON.parse(value)
		return { value, arguments }
	}
	async collectArray(amount, only) {
		const baseURL = this.local
			? "http://localhost:9090/stream-api/data-collecting"
			: "stream-api/data-collecting"
		let value
		switch (only) {
			case "arraylist":
				value = await ajax(`${baseURL}/arrayList?amount=${amount}`)
				break
			case "fixedarraylist":
				value = await ajax(`${baseURL}/fixedArrayList?amount=${amount}`)
				break
			case "linkedlist":
				value = await ajax(`${baseURL}/linkedList?amount=${amount}`)
				break
		}
		value = JSON.parse(value)
		return { value, arguments }
	}
	async ifPresent(amount, only) {
		const baseURL = this.local
			? ` http://localhost:9090/stream-api/optionalIfPresent`
			: "stream-api/optionalIfPresent"
		let before = null,
			after = null
		switch (only) {
			case "before":
				before = JSON.parse(await ajax(`${baseURL}/ifPresent?amount=${amount}`))
				break
			case "after":
				after = JSON.parse(await ajax(`${baseURL}/plain?amount=${amount}`))
				break
			default:
				before = JSON.parse(await ajax(`${baseURL}/ifPresent?amount=${amount}`))
				after = JSON.parse(await ajax(`${baseURL}/plain?amount=${amount}`))
				break
		}
		return { before, after }
	}
	async ofNullableOrElse(amount, only) {
		const baseURL = this.local
			? `http://localhost:9090/stream-api/optionalOfNullableOrElse`
			: "stream-api/optionalOfNullableOrElse"
		let before = null,
			after = null,
			example = null
		switch (only) {
			case "multi":
				example = JSON.parse(await ajax(`${baseURL}/multi?amount=${amount}`))
				break
			case "lambda":
				before = JSON.parse(await ajax(`${baseURL}/lambda?amount=${amount}`))
				break
			case "single":
				after = JSON.parse(await ajax(`${baseURL}/single?amount=${amount}`))
				break
		}
		return { example, before, after }
	}
	async isEmpty(amount, only, only2) {
		const baseURL = this.local
			? `http://localhost:9090/core-java/blank-string`
			: "core-java/blank-string"
		return {
			value: JSON.parse(
				await ajax(
					`${baseURL}/${only2}?amount=${amount}&generationType=${only}`
				)
			),
			args: arguments,
		}
	}
}
window.API = new JavaAPI()
